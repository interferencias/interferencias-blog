---
layout: page
title: JASYP '18 - Participación
permalink: /jasyp/18/registro/
description: "Formulario de participación para las JASYP '18"
image:
  feature: banners/header.jpg
timing: false
---

Las **JASYP** (*Jornadas sobre Anonimato, Seguridad y Privacidad*) son unas jornadas organizadas por [Interferencias](https://twitter.com/Inter_ferencias) con la colaboración de [Follow the White Rabbit](https://twitter.com/fwhibbit_blog) y [Hack&Beers](https://twitter.com/hackandbeers), que pretende dar un espacio a todas aquellas personas con inquietudes sobre los problemas que encontramos en el día a día cada vez que usamos Internet o cualquier ordenador en general. La edición de 2017 se celebró en la [Escuela Técnica Superior de Ingenierías Informática y de Telecomunicación de la Universidad de Granada](https://etsiit.ugr.es/), recibiendo a parcitipantes y asistentes de toda España durante dos días en el Salón de Actos de la propia Escuela, para cerrar con un tercer día en el que se celebró un **Hack&Beers** en el bar **La Posada**.

![cartel_privacidad_etsiit]({{ "/assets/images/jasyp/17/01.jpg" }})

En este 2018 pretendemos lanzar la segunda edición de este evento, donde esperamos poder aprender mucho, aprovechar los días incluso más, y sacar partido del buen rollo que Granada siempre brinda en sus bares. Para ello lanzamos este **Call for Papers** para todas aquellas personas interesadas en participar en la **edición de este año**, que tendrá lugar **los días VIERNES 13 Y SÁBADO 14 DE ABRIL**.

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Qué pedimos?</h3>
		<hr>
  </div>
</div>

- Personas interesadas en hablar sobre la importancia de los **derechos digitales**, la **privacidad en Internet**, la **seguridad informática** y todos aquellos temas de este ámbito que puedan tener relación.
- Las charlas pueden ser de cualquier área. Es cierto que la temática se presta a talleres y charlas técnicas, pero nos encantaría acoger una charla relacionada con la temática desde un punto de vista distinto, como puede ser el de personas dedicadas a **derecho, filosofía, política, arte** o cualquier otra rama. ¡Lánzate y cuéntanoslo!
- Si la charla está apoyada en Software Libre o cualquier contenido que sea libre, muchísimo mejor.
- Además, también vamos a organizar un concurso artístico sobre el que podréis encontrar más información [aquí]({{ site.url }}/jasyp/concurso/).
- Y como complemento a todo esto, también estamos preparando un **Capture The Flag**, donde cualquiera que se anime pueda poner a prueba sus habilidades a la hora de explotar las debilidades de sistemas informáticos (las bases de participación serán publicadas próximamente).

![cartel_privacidad_etsiit]({{ "/assets/images/jasyp/17/02.jpg" }})

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Qué ofrecemos?</h3>
		<hr>
  </div>
</div>

Los integrantes de Interferencias somos un grupo de personas con mucha energía e interés, pero por desgracia no contamos con la suficiente solvencia económica como para poder hacernos cargo de los gastos de viaje y alojamiento para los participantes que tengan que desplazarse. Sin embargo, podemos ofreceros cerveza y tapas gratis, además de mucho buen humor granadino.

Es por eso que también estamos buscando patrocinadores que nos ayuden a cubrir estos gastos que nos gustaría poder afrontar de cara a hacer un evento de una mayor magnitud. Puedes encontrar más información sobre cómo patrocinar las JASYP en [esta página]({{ site.url }}/jasyp/patrocinio/) o escribiendo al correo [jasyp@interferencias.tech](mailto:[jasyp@interferencias.tech]).

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Te hemos convencido?</h3>
		<hr>
  </div>
</div>

En el caso de sea así, si todo esto te suena bien, aquí te dejamos el formulario a rellenar; aunque también agradecemos enormemente que nos ayudes con la difusión del evento a través de redes sociales o el simple boca a boca. Aceptamos propuestas de participantes de todas las edades, géneros, etnias y condición social, **seas quien seas, si te gusta la temática ¡Anímate!**

![cartel_privacidad_etsiit]({{ "/assets/images/jasyp/17/03.jpg" }})
