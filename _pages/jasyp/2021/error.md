---
layout: page
title: Sala esLibre 2021&#58 Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)
permalink: /eslibre/error/
description: "Propuesta sala esLibre 2021&#58 Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)"
image:
  feature: banners/header.jpg
timing: false
share: false
---

Se ha producido un error al registrar tu propuesta.

Por favor, vuelve a intentarlo pasado unos minutos y en caso que el problema siga produciéndose escríbenos a [info@interferencias.tech](mailto:info@interferencias.tech).

Saludos,
![banner]({{ "/assets/images/social/tags/banner.png" }})
