---
layout: page
title: Sala esLibre 2021&#58 Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)
permalink: /eslibre/recibida/
description: "Propuesta sala esLibre 2021&#58 Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)"
image:
  feature: banners/header.jpg
timing: false
share: false
---

Gracias por registrarte en la sala **Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)** de **esLibre 2021**!!!

Tu solicitud de participación ha sido registrada con éxito. Deberías recibir en unos minutos un correo de confirmación de **tux@eslib.re** a la dirección que has indicado.

Si no lo recibes, tampoco se encuentra en tu carpeta de correo no deseado o tienes cualquier duda, por favor, escríbenos a [info@interferencias.tech](mailto:info@interferencias.tech) desde la misma dirección con la que has registrado la solicitud para comprobarlo.

Saludos,
![banner]({{ "/assets/images/social/tags/banner.png" }})
