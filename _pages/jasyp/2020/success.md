---
layout: page
title: Sala esLibre 2020&#58 Derechos Digitales, Privacidad en Internet y Seguridad Informática
permalink: /eslibre/2020/success/
description: "Propuesta sala esLibre 2020"
image:
  feature: banners/header.jpg
timing: false
share: false
---

Gracias por registrarte en la sala **Derechos Digitales, Privacidad en Internet y Seguridad Informática** de **esLibre 2020**!!!

Tu solicitud de participación ha sido registrada con éxito. Deberías recibir en unos minutos un correo de confirmación de [info@interferencias.tech](mailto:info@interferencias.tech) a la dirección que has indicado.

Si no lo recibes, tampoco se encuentra en tu carpeta de correo no deseado o tienes cualquier duda, por favor, escríbenos a [info@interferencias.tech](mailto:jasyp@interferencias.tech) desde la misma dirección con la que has registrado la solicitud para comprobarlo.

Saludos,
![banner]({{ "/assets/images/social/tags/banner.png" }})
