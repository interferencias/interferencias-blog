---
layout: page
title: JASYP '19 - Competición CTF
permalink: /jasyp/19/ctf/
description: "CTF de JASYP"
image:
  feature: banners/header.jpg
timing: false
share: false
---

![banner]({{ "/assets/images/jasyp/19/actividades/ctf.png" }})

Como en ediciones anterior, este año también se realizará una competición **CTF (Capture The Flag)** abierta, dirigida principalmente al público asistente al evento, pero de participación libre. Los jugadores tendrán que pasar varios niveles que **pondrán a prueba sus habilidades de hacking** en diferentes áreas. El CTF será diseñado por un grupo de personas integrado tanto por miembros de **Interferencias**, como por miembros de la comunidad [**Hacking Desde Cero**](https://www.hackingdesdecero.org/).

<figure>
  <a href="https://twitter.com/eduSatoe/status/984729890334367746"><img src="/assets/images/jasyp/18/ctf/edusatoe.jpg" alt="edusatoe"></a>
  <figcaption><a href="https://twitter.com/eduSatoe" target="_blank">Eduardo Sánchez</a>, fundador de Hack&Beers, durante la competición de CTF con sus alumnos del CFGM en Sistemas Microinformáticos y Redes</figcaption>
</figure>

Este año nos gustaría ofrecerle [un premio]({{ site.url }}/jasyp/patrocinio/) a la ganadora o el ganador del juego, con la intención de animar a la participación y alentar la sana competitividad entre los asistentes.

Este tipo de juegos son muy comunes en los eventos relacionados con la ciberseguridad, y después de la gran experiencia y participación del año pasado, prepararemos unos sistema de retos más completo aún.

<figure>
  <a href="https://twitter.com/hispasec/statuses/984785869520429056"><img src="/assets/images/jasyp/18/ctf/hispasec.jpeg" alt="hispasec"></a>
  <figcaption>Equipo de ingenieros en seguridad de Hispasec entre los que se encuentra el ganador de la última edición, <a href="https://twitter.com/devploit" target="_blank">Daniel Púa</a></figcaption>
</figure>
