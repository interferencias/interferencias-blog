---
layout: page
title: JASYP '19 - Participación
permalink: /jasyp/19/success/
description: "Formulario de participación para las JASYP '19"
image:
  feature: banners/header.jpg
timing: false
share: false
---

Gracias por registrarte en las **JASYP '19**!!!

Tu solicitud de participación ha sido registrada con éxito. Deberías recibir en unos minutos un correo de confirmación de [jasyp@interferencias.tech](mailto:jasyp@interferencias.tech) a la dirección que has indicado.

Si no lo recibes, tampoco se encuentra en tu carpeta de correo no deseado o tienes cualquier duda, por favor, escríbenos a [jasyp@interferencias.tech](mailto:jasyp@interferencias.tech) desde la misma dirección con la que has registrado la solicitud para comprobarlo.

Saludos,
![cartel_privacidad_etsiit]({{ "/assets/images/social/tags/banner.png" }})
