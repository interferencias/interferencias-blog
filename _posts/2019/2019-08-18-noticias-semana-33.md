---
layout: post
title: Noticias semana 12 al 18 de agosto de 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

Estas son las noticias más relevantes que hemos encontrado en nuestras redes sociales esta semana:

- **When Will We Get the Full Truth About How and Why the Government Is Using Face Recognition?**:
[https://www.eff.org/deeplinks/2019/07/when-will-we-get-full-truth-about-how-and-why-government-using-facial-recognition](https://www.eff.org/deeplinks/2019/07/when-will-we-get-full-truth-about-how-and-why-government-using-facial-recognition)
- **Amazon Requires Police to Shill Surveillance Cameras in Secret Agreement**:
[https://www.vice.com/en_us/article/mb88za/amazon-requires-police-to-shill-surveillance-cameras-in-secret-agreement](https://www.vice.com/en_us/article/mb88za/amazon-requires-police-to-shill-surveillance-cameras-in-secret-agreement)
- **Europa denuncia a España por no aplicar las normas de protección de datos en relación a la investigación de delitos**:
[https://derechodelared.com/europa-espana-proteccion-de-datos/](https://derechodelared.com/europa-espana-proteccion-de-datos/)
- **La tecnología o la vida: el derecho a la desconexión digital**:
[https://www.elsaltodiario.com/trabajo-digno-pension-justa/la-tecnologia-o-la-vida-el-derecho-a-la-desconexion-digital](https://www.elsaltodiario.com/trabajo-digno-pension-justa/la-tecnologia-o-la-vida-el-derecho-a-la-desconexion-digital)
- **Researchers: Anonymized data does little to protect user privacy**:
[https://thenextweb.com/insider/2019/07/30/anonymized-data-does-little-to-protect-privacy/](https://thenextweb.com/insider/2019/07/30/anonymized-data-does-little-to-protect-privacy/)
- **No existe el anonimato, gracias a tus datos pueden rastrearte y encontrarte**:
[https://www.abc.es/tecnologia/informatica/software/abci-no-existe-anonimato-gracias-datos-pueden-rastrearte-y-encontrarte-201907290226_noticia.html](https://www.abc.es/tecnologia/informatica/software/abci-no-existe-anonimato-gracias-datos-pueden-rastrearte-y-encontrarte-201907290226_noticia.html)
- **Facebook Just Paid a $5 Billion Fine for Privacy Breaches. Now it Wants Access to Your Brain**:
[https://www.vice.com/en_us/article/gy4zeq/facebook-just-paid-a-dollar5-billion-fine-for-privacy-breaches-now-it-wants-access-to-your-brain](https://www.vice.com/en_us/article/gy4zeq/facebook-just-paid-a-dollar5-billion-fine-for-privacy-breaches-now-it-wants-access-to-your-brain)
- **Twitter may have used your personal data for ads without your permission. Time to fix AdTech!**:
[https://privacyinternational.org/news-analysis/3111/twitter-may-have-used-your-personal-data-ads-without-your-permission-time-fix](https://privacyinternational.org/news-analysis/3111/twitter-may-have-used-your-personal-data-ads-without-your-permission-time-fix)
- **¿Es legal que te grabe la cámara de un coche?**:
[https://hackercar.com/es-legal-que-te-grabe-la-camara-de-un-coche/](https://hackercar.com/es-legal-que-te-grabe-la-camara-de-un-coche/)
- **Twitter admite haber usado datos de usuarios sin permiso**:
[https://www.cnet.com/es/noticias/twitter-comparte-datos-anunciantes-permiso/](https://www.cnet.com/es/noticias/twitter-comparte-datos-anunciantes-permiso/)
- **¿Qué tan seguros son los registros escolares? No mucho, dice investigador de seguridad estudiantil**:
[https://tecnoticias.net/2019/08/10/que-tan-seguros-son-los-registros-escolares-no-mucho-dice-investigador-de-seguridad-estudiantil/](https://tecnoticias.net/2019/08/10/que-tan-seguros-son-los-registros-escolares-no-mucho-dice-investigador-de-seguridad-estudiantil/)
- **¿Qué queda hoy del ciberanarquismo?**:
[https://www.lavanguardia.com/tecnologia/actualidad/20190810/463800612389/ciberanarquismo-etica-hacker-que-queda.html](https://www.lavanguardia.com/tecnologia/actualidad/20190810/463800612389/ciberanarquismo-etica-hacker-que-queda.html)
- **Spying on HTTPS**:
[https://textslashplain.com/2019/08/11/spying-on-https/](https://textslashplain.com/2019/08/11/spying-on-https/)
- **Cómo proteger las redes sociales y saber si han sido ‘hackeadas’**:
[https://elpais.com/tecnologia/2019/07/19/actualidad/1563548264_655411.html](https://elpais.com/tecnologia/2019/07/19/actualidad/1563548264_655411.html)
- **Google’s rivals opt out of search engine auction, calling it ‘unethical’ and ‘anti-competitive’**:
[https://venturebeat.com/2019/08/12/googles-rivals-opt-out-of-search-engine-auction-calling-it-unethical-and-anti-competitive/](https://venturebeat.com/2019/08/12/googles-rivals-opt-out-of-search-engine-auction-calling-it-unethical-and-anti-competitive/)
- **Google confirma que millones de móviles Android vienen con malware preinstalado**:
[https://www.adslzone.net/2019/08/12/google-apps-malware-preinstaladas-android/](https://www.adslzone.net/2019/08/12/google-apps-malware-preinstaladas-android/)
- **Rusia exigió a Google que no destaque videos de las protestas opositoras en YouTube**:
[https://www.infobae.com/america/mundo/2019/08/12/rusia-exigio-a-google-que-no-destaque-videos-de-las-protestas-opositoras-en-youtube/](https://www.infobae.com/america/mundo/2019/08/12/rusia-exigio-a-google-que-no-destaque-videos-de-las-protestas-opositoras-en-youtube/)
- **Nuestros datos personales, el filón de las empresas, que mueve millones de euros sin que nos demos cuenta**:
[https://cybersecuritynews.es/nuestros-datos-personales-el-filon-de-las-empresas-que-mueve-millones-de-euros-sin-que-nos-demos-cuenta/](https://cybersecuritynews.es/nuestros-datos-personales-el-filon-de-las-empresas-que-mueve-millones-de-euros-sin-que-nos-demos-cuenta/)
- **Study: Trump’s paid Peter Thiel’s Palantir $1.5B so far to build ICE’s mass-surveillance network**:
[https://thenextweb.com/artificial-intelligence/2019/08/12/study-trumps-paid-peter-thiels-palantir-1-5b-so-far-to-build-ices-mass-surveillance-network/](https://thenextweb.com/artificial-intelligence/2019/08/12/study-trumps-paid-peter-thiels-palantir-1-5b-so-far-to-build-ices-mass-surveillance-network/)
- **Trump quiere censurar la libertad de expresión en Internet con un nuevo plan**:
[https://www.adslzone.net/2019/08/13/nuevo-plan-trump-censurar-libertad-expresion-internet](https://www.adslzone.net/2019/08/13/nuevo-plan-trump-censurar-libertad-expresion-internet)
- **Facebook pagó a contratistas externos para transcribir clips de audio de Messenger**:
[https://www.cnet.com/es/noticias/facebook-pago-contratistas-para-transcribir-audio-chat-messenger/](https://www.cnet.com/es/noticias/facebook-pago-contratistas-para-transcribir-audio-chat-messenger/)
- **Países que espían a sus ciudadanos: Kazajistán toma el control de los dispositivos de su población**:
[https://cybersecuritynews.es/paises-que-espian-a-sus-ciudadanos-kazajistan-toma-el-control-de-los-dispositivos-de-su-poblacion/](https://cybersecuritynews.es/paises-que-espian-a-sus-ciudadanos-kazajistan-toma-el-control-de-los-dispositivos-de-su-poblacion/)
- **A ‘big data’ firm sells Cambridge Analytica’s methods to global politicians, documents show**:
[https://qz.com/1666776/data-firm-ideia-uses-cambridge-analytica-methods-to-target-voters/](https://qz.com/1666776/data-firm-ideia-uses-cambridge-analytica-methods-to-target-voters/)
- **Un fallo de seguridad expone 27,8 millones de registros de datos biométricos**:
[https://www.europapress.es/portaltic/ciberseguridad/noticia-fallo-seguridad-expone-278-millones-registros-datos-biometricos-20190814142303.html](https://www.europapress.es/portaltic/ciberseguridad/noticia-fallo-seguridad-expone-278-millones-registros-datos-biometricos-20190814142303.html)
- **La solución de Chrome para evitar que las webs sepan que usamos el modo incógnito realmente no solucionó nada**:
[https://www.genbeta.com/navegadores/solucion-chrome-para-evitar-que-webs-sepan-que-usamos-modo-incognito-realmente-no-soluciono-nada](https://www.genbeta.com/navegadores/solucion-chrome-para-evitar-que-webs-sepan-que-usamos-modo-incognito-realmente-no-soluciono-nada)
- **Major breach found in biometrics system used by banks, UK police and defence firms**:
[https://www.theguardian.com/technology/2019/aug/14/major-breach-found-in-biometrics-system-used-by-banks-uk-police-and-defence-firms](https://www.theguardian.com/technology/2019/aug/14/major-breach-found-in-biometrics-system-used-by-banks-uk-police-and-defence-firms)
- **Los semáforos chinos detectan a quienes no cruzan debidamente y exponen su caras públicamente para avergonzarles**:
[https://www.xataka.com/privacidad/semaforos-chinos-detectan-a-quienes-no-cruzan-debidamente-exponen-su-caras-publicamente-para-avergonzarles](https://www.xataka.com/privacidad/semaforos-chinos-detectan-a-quienes-no-cruzan-debidamente-exponen-su-caras-publicamente-para-avergonzarles)
- **Google nos encierra en Google: los usuarios no hacen clic ni en la mitad de las búsquedas**:
[https://www.xataka.com/servicios/google-nos-encierra-google-usuarios-no-hacen-clic-mitad-busquedas](https://www.xataka.com/servicios/google-nos-encierra-google-usuarios-no-hacen-clic-mitad-busquedas)
- **Trump asks Congress to permanently reauthorize NSA's expiring power to access domestic call records**:
[https://boingboing.net/2019/08/15/nsa-zombie-revived.html/](https://boingboing.net/2019/08/15/nsa-zombie-revived.html/)
- **Microsoft admite que escucha algunas grabaciones de Skype y Cortana**:
[https://www.20minutos.es/noticia/3733636/0/microsoft-admite-escucha-grabaciones-skype-cortana/](https://www.20minutos.es/noticia/3733636/0/microsoft-admite-escucha-grabaciones-skype-cortana/)
- **Que es la ciberinteligencia y por qué es necesaria**:
[https://www.arpsecure.com/blog/que-es-la-ciberinteligencia-y-porque-es-necesaria](https://www.arpsecure.com/blog/que-es-la-ciberinteligencia-y-porque-es-necesaria)
- **Cómo Huawei ayudó a gobiernos africanos a espiar a sus rivales políticos**:
[https://www.infobae.com/america/tecno/2019/08/15/como-huawei-ayudo-a-gobiernos-africanos-a-espiar-a-sus-rivales-politicos/](https://www.infobae.com/america/tecno/2019/08/15/como-huawei-ayudo-a-gobiernos-africanos-a-espiar-a-sus-rivales-politicos/)
- **Amazon Uses a Twitter Army of Employees to Fight Criticism of Warehouses**:
[https://www.nytimes.com/2019/08/15/style/amazon-fc-ambassadors.html](https://www.nytimes.com/2019/08/15/style/amazon-fc-ambassadors.html)
- **LGBTQ YouTubers are suing YouTube over alleged discrimination**:
[https://www.theverge.com/2019/8/14/20805283/lgbtq-youtuber-lawsuit-discrimination-alleged-video-recommendations-demonetization](https://www.theverge.com/2019/8/14/20805283/lgbtq-youtuber-lawsuit-discrimination-alleged-video-recommendations-demonetization)
- **Hundreds of Thousands of People Are Using Passwords That Have Already Been Hacked, Google Says**:
[https://www.vice.com/en_us/article/zmjvm9/hundreds-of-thousands-of-people-are-using-passwords-that-have-already-been-hacked-google-says](https://www.vice.com/en_us/article/zmjvm9/hundreds-of-thousands-of-people-are-using-passwords-that-have-already-been-hacked-google-says)
- **Los embajadores de Amazon en Twitter están desdibujando la línea entre realidad y ficción**:
[https://tecnoticias.net/2019/08/16/los-embajadores-de-amazon-en-twitter-estan-desdibujando-la-linea-entre-realidad-y-ficcion/](https://tecnoticias.net/2019/08/16/los-embajadores-de-amazon-en-twitter-estan-desdibujando-la-linea-entre-realidad-y-ficcion/)
- **Trump quiere extender el programa de vigilancia telefónica de la NSA**:
[https://www.cnet.com/es/noticias/trump-extender-programa-de-vigilancia-telefonica-de-la-nsa/](https://www.cnet.com/es/noticias/trump-extender-programa-de-vigilancia-telefonica-de-la-nsa/)
- **La información personal que revelamos al compartir nuestro número telefónico**:
[https://www.nytimes.com/es/2019/08/16/espanol/ciencia-y-tecnologia/datos-telefono-informacion-seguridad.html](https://www.nytimes.com/es/2019/08/16/espanol/ciencia-y-tecnologia/datos-telefono-informacion-seguridad.html)
- **Descubierto un fallo de seguridad que afecta a dispositivos con Bluetooth**:
[https://www.tuexperto.com/2019/08/16/descubierto-un-fallo-de-seguridad-que-afecta-a-dispositivos-con-bluetooth/](https://www.tuexperto.com/2019/08/16/descubierto-un-fallo-de-seguridad-que-afecta-a-dispositivos-con-bluetooth/)
- **Tras Google, Amazon y Apple, Facebook también admite haber estado escuchando audios de sus usuarios**:
[https://www.genbeta.com/redes-sociales-y-comunidades/google-amazon-apple-facebook-tambien-admite-haber-estado-escuchando-audios-sus-usuarios](https://www.genbeta.com/redes-sociales-y-comunidades/google-amazon-apple-facebook-tambien-admite-haber-estado-escuchando-audios-sus-usuarios)

### Recomendado:
- **KNOB (Key Negotiation of Bluetooth) Attack**, página con toda la información sobre la última vulnerabilidad encontrada en el protocolo de comunicaciones de Bluetooth: [https://knobattack.com/](https://knobattack.com/)

Recuerda que además de poder seguirnos en [Twitter](https://twitter.com/Inter_ferencias) y [Mastodon](https://mastodon.technology/@interferencias), te invitamos a participar y comentar sobre todo esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias).
