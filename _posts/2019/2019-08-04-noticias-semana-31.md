---
layout: post
title: Noticias semana 29 de julio al 4 agosto de 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

Estas son las noticias más relevantes que hemos encontrado en nuestras redes sociales esta semana:

- **DARPA Is Building a $10 Million, Open Source, Secure Voting System**: [https://www.vice.com/en_us/article/yw84q7/darpa-is-building-a-dollar10-million-open-source-secure-voting-system](https://www.vice.com/en_us/article/yw84q7/darpa-is-building-a-dollar10-million-open-source-secure-voting-system)
- **I Hacked an Election. So Can the Russians**: [https://www.nytimes.com/2018/04/05/opinion/election-voting-machine-hacking-russians.html](https://www.nytimes.com/2018/04/05/opinion/election-voting-machine-hacking-russians.html)
- **Testimonials about the encrypted contact form Secure Connect**: [https://tutanota.com/blog/posts/secure-connect-encrypted-contact-form-testimonial/](https://tutanota.com/blog/posts/secure-connect-encrypted-contact-form-testimonial/)
- **El Gobierno de Australia creará una oficina especial para vigilar a Google y Facebook**: [https://www.fayerwayer.com/2019/07/gobierno-australia-google-facebook/](https://www.fayerwayer.com/2019/07/gobierno-australia-google-facebook/)
- **In Hong Kong Protests, Faces Become Weapons**: [https://www.nytimes.com/2019/07/26/technology/hong-kong-protests-facial-recognition-surveillance.html](https://www.nytimes.com/2019/07/26/technology/hong-kong-protests-facial-recognition-surveillance.html)
- **El smartphone será tu carné de conducir en España**: [https://elandroidelibre.elespanol.com/2019/07/el-smartphone-sera-tu-carne-de-conducir-en-espana.html](https://elandroidelibre.elespanol.com/2019/07/el-smartphone-sera-tu-carne-de-conducir-en-espana.html)
- **The Encryption Debate Is Over - Dead At The Hands Of Facebook**: [https://www.forbes.com/sites/kalevleetaru/2019/07/26/the-encryption-debate-is-over-dead-at-the-hands-of-facebook/](https://www.forbes.com/sites/kalevleetaru/2019/07/26/the-encryption-debate-is-over-dead-at-the-hands-of-facebook/)
- **GitHub ha comenzado a bloquear las cuentas de los desarrolladores que se encuentran en países sancionados por EEUU**: [https://www.genbeta.com/actualidad/github-ha-comenzado-a-bloquear-cuentas-desarrolladores-que-se-encuentran-paises-sancionados-eeuu](https://www.genbeta.com/actualidad/github-ha-comenzado-a-bloquear-cuentas-desarrolladores-que-se-encuentran-paises-sancionados-eeuu)
- **Biometric data becomes new weapon in Hong Kong protests**: [https://www.pbs.org/newshour/show/biometric-data-becomes-new-weapon-in-hong-kong-protests](https://www.pbs.org/newshour/show/biometric-data-becomes-new-weapon-in-hong-kong-protests)
- **Bastan tres datos para identificar a cualquiera en una base anónima**: [https://www.technologyreview.es/s/11326/bastan-tres-datos-para-identificar-cualquiera-en-una-base-anonima](https://www.technologyreview.es/s/11326/bastan-tres-datos-para-identificar-cualquiera-en-una-base-anonima)
- **It's official: Deploying Facebook's 'Like' button on your website makes you a joint data slurper**: [https://www.theregister.co.uk/2019/07/29/eu_gdpr_facebook_like_button/](https://www.theregister.co.uk/2019/07/29/eu_gdpr_facebook_like_button/)
- **Nueva versión de Trickbot afecta a Windows Defender**: [https://unaaldia.hispasec.com/2019/07/nueva-version-de-trickbot-afecta-a-windows-defender.html](https://unaaldia.hispasec.com/2019/07/nueva-version-de-trickbot-afecta-a-windows-defender.html)
- **Hackean los datos de más de 100 millones de clientes del banco Capital One**: [https://www.eleconomista.es/empresas-finanzas/noticias/10018554/07/19/Hackean-los-datos-de-mas-de-100-millones-de-clientes-del-banco-Capital-One.html](https://www.eleconomista.es/empresas-finanzas/noticias/10018554/07/19/Hackean-los-datos-de-mas-de-100-millones-de-clientes-del-banco-Capital-One.html)
- **El TJUE impedirá que las páginas web te puedan rastrear a través del botón 'me gusta' de Facebook**: [https://www.genbeta.com/actualidad/tjue-impedira-que-paginas-web-te-puedan-rastrear-a-traves-boton-me-gusta-facebook](https://www.genbeta.com/actualidad/tjue-impedira-que-paginas-web-te-puedan-rastrear-a-traves-boton-me-gusta-facebook)
- **¿Qué es la criptografía poscuántica y por qué se volverá impresdincible?**: [https://www.technologyreview.es/s/11310/que-es-la-criptografia-poscuantica-y-por-que-se-volvera-impresdincible](https://www.technologyreview.es/s/11310/que-es-la-criptografia-poscuantica-y-por-que-se-volvera-impresdincible)
- **Calls for backdoor access to WhatsApp as Five Eyes nations meet**: [https://www.theguardian.com/uk-news/2019/jul/30/five-eyes-backdoor-access-whatsapp-encryption](https://www.theguardian.com/uk-news/2019/jul/30/five-eyes-backdoor-access-whatsapp-encryption)
- **“Los coches autónomos podrían hackearse para provocar atentados”**: [https://www.agenciasinc.es/Entrevistas/Los-coches-autonomos-podrian-hackearse-para-provocar-atentados2](https://www.agenciasinc.es/Entrevistas/Los-coches-autonomos-podrian-hackearse-para-provocar-atentados2)
- **Google’s Plans for Chrome Extensions Won’t Really Help Security**: [https://www.eff.org/deeplinks/2019/07/googles-plans-chrome-extensions-wont-really-help-security](https://www.eff.org/deeplinks/2019/07/googles-plans-chrome-extensions-wont-really-help-security)
- **Los manifestantes de Hong Kong empiezan a usar láseres contra la policía para evadir los sistemas de reconocimiento facial**: [https://www.xataka.com/inteligencia-artificial/manifestantes-hong-kong-empiezan-a-usar-laseres-policia-para-evadir-sistemas-reconocimiento-facial](https://www.xataka.com/inteligencia-artificial/manifestantes-hong-kong-empiezan-a-usar-laseres-policia-para-evadir-sistemas-reconocimiento-facial)
- **New Warning Issued Over Google's Chrome Ad-Blocking Plans**: [https://www.forbes.com/sites/kateoflahertyuk/2019/08/01/warning-issued-over-google-chrome-ad-blocking-plans](https://www.forbes.com/sites/kateoflahertyuk/2019/08/01/warning-issued-over-google-chrome-ad-blocking-plans)
- **Facebook trabaja en una herramienta para acabar con el cifrado de extremo a extremo de WhatsApp**: [https://www.xatakamovil.com/aplicaciones/facebook-trabaja-herramienta-para-acabar-cifrado-extremo-a-extremo-whatsapp](https://www.xatakamovil.com/aplicaciones/facebook-trabaja-herramienta-para-acabar-cifrado-extremo-a-extremo-whatsapp)
- **Autodefensa Informática 27: “Sé lo que hicisteis la segunda temporada” (Selección estival)**: [https://autodefensainformatica.radioalmaina.org/autodefensa-informatica-27-se-lo-que-hicisteis-la-segunda-temporada-seleccion-estival/](https://autodefensainformatica.radioalmaina.org/autodefensa-informatica-27-se-lo-que-hicisteis-la-segunda-temporada-seleccion-estival/)
- **FaceApp solicita innecesariamente el acceso a la lista de amigos de Facebook**: [https://unaaldia.hispasec.com/2019/08/faceapp-solicita-innecesariamente-el-acceso-a-la-lista-de-amigos-de-facebook.html](https://unaaldia.hispasec.com/2019/08/faceapp-solicita-innecesariamente-el-acceso-a-la-lista-de-amigos-de-facebook.html)
- **Así es cómo podremos elegir navegador y motor de búsqueda en Android a partir de 2020 en la UE**: [https://www.genbeta.com/buscadores/asi-como-podremos-elegir-navegador-motor-busqueda-android-a-partir-2020-ue](https://www.genbeta.com/buscadores/asi-como-podremos-elegir-navegador-motor-busqueda-android-a-partir-2020-ue)

### Recomendado:
- **Net Neutrality Defense Guide: Summer 2019 Edition**: [https://www.eff.org/net-neutrality-defense-2019-edition](https://www.eff.org/net-neutrality-defense-2019-edition)

### Redes sociales:
- **Manifestantes en Hong Kong usando lásers para impedir que la policía pueda utilizar reconocimiento facial"**: [https://mastodon.social/@skynebula/102530472639711562](https://mastodon.social/@skynebula/102530472639711562)

- **Hilo muy interesante de [Críptica](https://www.criptica.org/) sobre el problema con Facebook y la privacidad**: [https://twitter.com/CripticaOrg/status/1155791070988517376](https://twitter.com/CripticaOrg/status/1155791070988517376)

Recuerda que además de poder seguirnos en [Twitter](https://twitter.com/Inter_ferencias) y [Mastodon](https://mastodon.technology/@interferencias), te invitamos a participar y comentar sobre todo esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias).
