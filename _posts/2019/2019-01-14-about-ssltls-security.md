---
layout: post
title:  About SSL/TLS security
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad linux
---

When we transmit sensitive data through the network, we want to feel secure doing it. For that, we use HTTPS traffic, which is HTTP protocol secured via an SSL/TLS. It checks confidentiality and authentication, this last one is done through [digital certificates](https://dev.to/terceranexus6/security-sprint-week-4-ca-pairs-creation-with-openssl-eia). Let's explore how to test vulnerabilities surrounding HTTP protocol transmission. What are we going to use?

### Openssl

As explained in the [official site](https://www.openssl.org/), openssl is a software library for applications that secure communications over computer networks against eavesdropping or need to identify the party at the other end. It's usually included in linux systems, but -in debian based systems- can be installed using `sudo apt install openssl`.

### nmap

nmap is a security scanner used to discover hosts and services on a computer network. In debian you can install it using `sudo apt install nmap`. This is a widely used tool for security, so if you don't have it and you are into cybersec, it's a **must-install**.

Okay let's do this. What are we going to look for when testing SSL/TLS security? First of all we are going to check if there are weak ciphers or protocols. As explained in [OWASP wiki](https://www.owasp.org/index.php/Testing_for_Weak_SSL/TLS_Ciphers,_Insufficient_Transport_Layer_Protection_(OTG-CRYPST-001)) about this topic, anonymous Diffie-Hellmann is a good example of authentication vulnerability, or SSLv2 protocol. With nmap we can scan a host using, for example `nmap [target]`; a ping only scan usign `nmap -sn [target]`; TCP SYN ping using `nmap -PS [target]`. We are foing to use `-sV` wich is the standard service scan.

```
nmap -sV www.example.com
```

We can use other custom options such as `-PN`, `--top-ports` and others. Anyway, with this last command we are able to identify the SSL service. Now we can check the certificate information, weak ciphers and SSLv2 each port. We are checking 443,465,993,995 in the example, but this should be customized in each case.

```
nmap --script ssl-cert,ssl-enum-ciphers -p 443,465,993,995 www.example.com
```

Finally, openssl is used for testing manually SSL/TLS trying to initiate a renegotiation by client connecting to server with openssl.

<img src="{{ site.url }}/assets/images/dev.to/fresh.gif" style="display: block; margin: 0 auto;">

```
openssl s_client -connect www.example.com:443
```

I created a simple [gist](https://gist.github.com/terceranexus6/420cf35fdcbb87633c7aa8a5731ef26f) for automating all of this using default configuration and customized host. It saves the output in three different files for analysis. Here's the code:

```
#!/bin/bash

BLUE='\033[0;36m'
RED='\033[0;31m'
NC='\033[0m' # No Color

#based in OWASP wiki

if [[ -z "$1" || -z "$2" ]]; then
   echo "./tls.sh host directory"

else

    mkdir $2
    cd $2

    echo -e "${BLUE}Checking for Client-initiated Renegotiation and Secure Renegotiation via openssl...${NC}"
    openssl s_client -connect $1:443  > negotiations

    if grep -q 'Secure Renegotiation IS NOT supported' "negotiations"; then
      echo -e "${RED}Secure Renegotiation IS NOT supported.${NC}"
    else
      echo -e "${BLUE}Certificate validity ensured.${NC}"
    fi

    echo -e "${BLUE}Checking for Certificate information, Weak Ciphers and SSLv2 via nmap...${NC}"
    nmap --script ssl-cert,ssl-enum-ciphers -p 443,465,993,995 $1 > ciphernmap

    if grep -q SSLv2 "ciphernmap"; then
      echo -e "${RED}Weak protocol found (SSLv2).${NC}"
    else
      echo -e "${BLUE}No weak protocol found.${NC}"
    fi

    echo -e "${BLUE}SSL service recognition via nmap...${NC}"
    nmap -sV --reason -PN -n --top-ports 100 $1 > nmapsslservice

    echo -e "${BLUE}Done.${NC}"

    echo -e "${RED}Don't forget to manually check the files created in case of doubt. Check OWASP wiki for more information.${NC}"

fi
```

For more information please heck the amazing [wiki of OWASP](https://www.owasp.org/index.php/Testing_for_Weak_SSL/TLS_Ciphers,_Insufficient_Transport_Layer_Protection_(OTG-CRYPST-001)).

*Also written in: [https://dev.to/terceranexus6/about-ssltls-security-48fj](https://dev.to/terceranexus6/about-ssltls-security-48fj)*
