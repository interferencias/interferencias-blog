---
layout: post
title:  Charla/taller "Introducción al desarrollo Blockchain"
author: germaaan
image:
  feature: banners/header.jpg
tags: eventos charla taller blockchain
---

<p style="text-align:center;"><img src="/assets/images/blockchain.v2.png" alt="taller_blockchain" height="500" width="500"></p>

En esta ocasión, desde Interferencias os queremos presentar un taller que hemos organizado de la mano de Lightstreams (<https://www.lightstreams.network/>), una empresa dedicada al **desarrollo de una plataforma descentralizada basada en la tecnología blockchain**, y que además tiene un **gran compromiso con la importancia de la privacidad y el software libre**.

### Descripción:

Más allá de la especulación y las criptomonedas (Bitcoin, Ethereum, EOS...), **blockchain ha traído una nueva tecnología y filosofía de desarrollo**, donde la transparencia, la seguridad y el software libre son los pilares fundamentales.

La charla comenzará explicando los orígenes del blockchain y su posterior popularización causada por las conocidas criptomonedas y ICOs. Se explicarán los principales fundamentos tecnológicos detrás del blockchain, además de las formas de desarrollo en las que se basa esta tecnología.

Al final de la charla se explicará **cómo crear tu propia aplicación blockchain, o aplicación descentralizada (DApp), en unos pocos pasos**.

Parte de la presentación será explicada en inglés, pero en todo momento se podrán resolver dudas en español sin problema.

### Empresa: ​[Lightstreams](https://www.lightstreams.network/)

**Lightstreams** es una pequeña startup del mundo blockchain que comenzó en el año 2017. Lightstreams busca a través de la creación de su propio blockchain y del posterior desarrollo de aplicaciones distribuidas (DApps), mejorar la privacidad y seguridad de la información y la distribución de la propiedad intelectual.

### Ponentes:

- **Lukas Lukac**: ​ Experimentado desarrollador especializado en desarrollos distribuidos y sistemas de alta demanda. Líder técnico en Trivago durante más de 3 años y co-fundador de TiltBook, una de las redes sociales para jugadores de poker más populares con más de 20k miembros activos. <br>**LinkedIn**: <https://www.linkedin.com/in/llukac>

- **Gabriel Garrido**:​ Ex-ingeniero de software en Amazon y arquitecto de software durante más 5 años en trivago. Experto en algoritmos de alto rendimiento y bases de datos distribuidas, con extensa formación académica. <br>**LinkedIn**: <https://www.linkedin.com/in/ggarri-86>

Registro no obligatorio: [https://www.meetup.com/es-ES/Granada-Geek/events/259087052/](https://www.meetup.com/es-ES/Granada-Geek/events/259087052/).
