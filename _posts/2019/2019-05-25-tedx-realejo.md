---
layout: post
title:  TEDx Realejo 2019&#58 ¿Qué consecuencias tiene subir una foto a Internet?
author: germaaan
image:
  feature: banners/header.jpg
tags: charlas
---

*TED* es una organización estadounidense sin fines de lucro dedicada a promover charlas divulgativas en formato charla corta sobre diferentes temáticas de interés para la sociedad en general.

Este año se realizó el primer de este tipo en Granada, **TEDx Realejo**, y Paula participó con la charla **¿Qué consecuencias tiene subir una foto a Internet?**:

<iframe width="560" height="315" src="https://www.youtube.com/embed/HiSpK36RN5w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
