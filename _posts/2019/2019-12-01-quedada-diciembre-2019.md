---
layout: post
title: Quedada Diciembre 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: quedadas
---

Volvemos a reunirnos para hablar de diversos temas de actualidad en el mundo de los **derechos digitales**, nuestra batalla a favor de la **privacidad y seguridad** en el mundo tecnológico y la lucha por el **software libre**.

<img src="{{ site.url }}/assets/images/quedadas/quedada_diciembre_2019.jpg" style="display: block; margin: 0 auto;">

Esta vez queremos hacer la quedada un poco más organizada con la intención de llegar a más gente interesada en las temáticas sobre las que nos interesa debatir para conocer más puntos de vista y seguramente enriquecernos culturalmente. Por eso, en esta ocasión la celebraremos en la [**Biblioteca Social “Hermanos Quero”**](https://www.bsquero.net/), un espacio en el cual desde hace muchos años se realizan un gran cantidad de actividades organizadas por diversos colectivos y movimientos sociales de Granada (las cuales podéis también encontrar en [**Agenda Almaina**](https://agenda.radioalmaina.org/)), y que también es sede de [**Radio Almaina**](https://radioalmaina.org/), una radio libre, autogestionada y asamblearia de Granada con el objetivo de dar difusión a las luchas sociales y la cultura crítica.

<a name="top"></a>
Como siempre, las quedadas son abiertas a todo el mundo y cualquier persona puede proponer un tema sobre el que debatir. En esta ocasión, después de hacer una breve introducción sobre qué es Interferencias y cuales son las cuestiones que consideramos que la gente debería tener conocimiento en los temas de **derechos digitales**, estos son posibles temas sobre los que debatir:

- **[Aprobación del "decretazo digital" para controlar las redes de comunicación](#primera)**
- **[Monitorización de las personas por organismos públicos usando sus smartphones](#segunda)**
- **[La problemática para la privacidad de la inexistencia de smartphones libres](#tercera)**
- **[Compra de publicidad política en redes sociales y algoritmos que nos gobiernan](#cuarta)**
- **[Existencia digital de las personas en un mundo "falseable" mediante tecnología](#quinta)**
- **[El Internet actual también puede sufrir de la especulación de "los mercados"](#sexta)**
- **[Regalando cacharros espía por Navidad](#septima)**
- **[El problema de que las personas "responsables" sean "irresponsables" en cuanto a seguridad informática](#octava)**

### <a name="primera"></a>Aprobación del "decretazo digital" para controlar las redes de comunicación

La aprobación del "decretazo digital" que el gobierno español ha llevado a cabo para que entremos en el selecto grupo de países que promulga leyes que permiten al Gobierno hacerse cargo (y cortar) los servicios de redes y comunicaciones sin revisión judicial por motivos de "orden público" (lo que en otros países ha llevado a [situaciones demenciales](https://www.nytimes.com/2019/12/01/world/middleeast/iran-protests-deaths.html)).

**¿Se usaría "decretazo digital" para interponerse en manifestaciones legítimas, pero que fueran contra [decisiones "polémicas" de un gobierno](https://www.elsaltodiario.com/opinion/contra-la-ley-mordaza-digital)? ¿Es este un camino que, de la mano de la "[industria de la vigilancia](https://www.theguardian.com/commentisfree/2019/nov/26/surveillance-industry-suppression-spyware)", tenga únicamente como objetivo la represión política? ¿Tendrá la gente que seguir buscando formas "[creativas](https://www.genbeta.com/redes-sociales-y-comunidades/tiktok-bloquea-a-usuaria-viralizacion-su-denuncia-campos-detencion-china)" de realizar protestas?**

[Arriba](#top)

### <a name="segunda"></a>Monitorizacón de las personas por organismos públicos usando sus smartphones

Recientemente el Instituto Nacional de Estadística [anuncio que monitorizaría la posición de los teléfonos móviles durante una semana para llevar a cabo un estudio sobre cómo se mueven los españoles](https://www.xataka.com/privacidad/operadoras-cederan-datos-localizacion-movil-al-ine-para-estudiar-donde-viven-trabajan-como-se-mueven-ciudadanos) en vista de optimizar infraestructuras públicas y otros servicios. Esto se llevó a cabo sin necesitar el consentimiento expreso de las personas (que además, en función de la compañía que tuvieran contratada tendrían serios problemas para negarse a "participar").

Hay ya algunos [estudios que ponen entredicho la posibilidad de que los datos anonimizados puedan ser realmente anónimos](https://techcrunch.com/2019/07/24/researchers-spotlight-the-lie-of-anonymous-data/) en función del origen, pero más allá de esto, **¿[se está legitimando desde los entes que deberían velar por la protección de nuestros datos personales que se haga negocio con dichos datos](https://www.elconfidencial.com/tecnologia/2019-10-30/ine-movistar-telefonica-vodafone-orange_2304595/)? ¿concebimos un futuro donde realmente se compartieron otros datos como nuestros datos financieros y de salud desde el propio Gobierno?**

[Arriba](#top)

### <a name="tercera"></a>La problemática para la privacidad de la inexistencia de smartphones libres

No es posible la privacidad sin el software libre ya que aunque decidiéramos no hacerla valer, tendríamos la oportunidad de conocer exactamente qué hacen y cómo funcionan nuestros dispositivos a la hora de gestionar nuestras comunicaciones. El problema que nos encontramos aquí es la poca "oferta" que nos encontramos en los smartphones, donde las soluciones de la comunidad solo tienen capacidad para darle cabida a una muy pequeña parte de los dispositivos existentes y alternativas como Purism Librem 5 parece que no han terminado de cuajar.

**¿Estamos en un escenario tan desolador para la privacidad en los smartphones? ¿La única alternativa para minimizar el impacto de un smartphone en nuestra privacidad es precisamente eliminar su parte de smart? ¿Todo está perdido y no nos quedará más remedio que confiar en "ingenios" como [este](https://www.xataka.com/privacidad/este-cargador-se-conecta-al-movil-para-generar-datos-falsos-que-anunciantes-acaban-confundidos) para paliar el problema?** (Por no decir los países como Rusia que van a prohibir la venta de smartphones, TVs y ordenadores que no vengan con software ruso preinstalado)

[Arriba](#top)

### <a name="cuarta">Compra de publicidad política en redes sociales y algoritmos que nos gobiernan

Las redes sociales forman parte casi inseparable de las vidas de muchas personas, personas que en muchos casos también las usan como fuente de información principal, con las ventajas y, más que desventajas, directamente peligros que eso conlleva. La importancia de la manipulación en las redes sociales tuvo un antes y un después tras el conocimiento de Cambridge Analytica; este debate ha llevado a que las redes sociales más usadas hayan decidido posicionarse sobre la venta de publicidad política: [Twitter va a prohibirla totalmente](https://www.elconfidencial.com/tecnologia/2019-10-30/twitter-prohibe-propaganda-politica_2307480/) y Facebook simplemente ha dicho que dará una mayor transparencia sobre la misma, pero que [defiende que se pueda comprar publicidad política con información falsa](https://www.genbeta.com/redes-sociales-y-comunidades/mark-zuckerberg-sigue-defendiendo-derecho-facebook-a-publicar-anuncios-politicos-informacion-falsa).

**¿Es la publicidad política esto algo que no está regulado por ser demasiado nuevo o pensamos que ha llegado el punto en el que pueden tener una injerencia real en la capacidad de decisión de las personas? Si no sabemos si podemos confiar en lo que pensamos esté siendo manipulado, ¿debemos empezar a preocuparnos porque [nos gobiernen los algoritmos](https://retina.elpais.com/retina/2019/11/22/tendencias/1574448048_476000.html)?** (Y la dependencia de que esos algoritmos sean "controlado" por [otros algoritmos](https://www.agenciasinc.es/Noticias/Nuevos-algoritmos-vigilan-el-buen-comportamiento-de-la-inteligencia-artificial))

[Arriba](#top)

### <a name="quinta">Existencia digital de las personas en un mundo "falseable" mediante tecnología

También relacionado con Twitter, después de conocerse que iba a empezar a cerrar las cuentas inactivas, ha tenido que dar marcha atrás después de las numerosas protestas que se habían planteado sobre las cuentas de personas fallecidas; al margen de perder "recuerdos" de personas famosas, muchas personas temen perder algo realmente irreplanzable, los recuerdos familiares cercanos.

**¿Hasta qué punto las redes sociales han llegado a ese punto de importancia en nuestra representación como personas y relación con las mismas?** Si eso hace indicar que le damos tanta importancia al mundo digital, **¿estamos preparados para vivir en un mundo en el que quizás no podamos saber qué es real y que no si los [deepfakes pueden suplantar a cualquier persona](https://es.gizmodo.com/richard-nixon-anuncia-que-los-astronautas-del-apolo-11-1840069422) y pueden clonar nuestra voz con solo [una muestra de 5 segundos](https://www.microsiervos.com/archivo/ia/clonar-voz-muestra-5-segundos-facil-increible-terrorifico.html)?**

[Arriba](#top)

### <a name="sexta">El Internet actual también puede sufrir de la especulación de "los mercados"

Parece que en el mundo actual practicamente todo tiene un precio, e Internet no iba a ser distinto. La ICANN, el organismo que gestiona todos los dominios de internet, eliminó las restricciones del coste de los dominios .org, en la practica esto permite que su gestor pueda ponerle el precio que desee a dichos dominios sin que nadie pueda impedirlo; tenemos que tener en cuenta que los .org tradicionalmente han sido usados por ONGs y proyectos de software libre tan importantes como [Wikipedia](https://es.wikipedia.org/wiki/Wikipedia:Portada). El problema ha venido cuando [la propiedad de estos dominios ha terminado en manos de una empresa de capital de riesgo](https://www.xataka.com/legislacion-y-derechos/incertidumbre-polemica-para-futuro-dominios-org-que-acaban-caer-manos-desconocida-ethos-capital). Esto ha hecho que personas de relevancia en el tema hayan mostrado su preocupación al respecto, como [Tim Berners-Lee, el padre de la World Wide Web](https://www.cnet.com/news/profit-priorities-could-taint-org-domain-web-inventor-tim-berners-lee-worries/), quien por otra parte recientemente [presentó su contrato social para construir una web libre, abierta y que sea respetuosa con la privacidad de los usuarios](https://contractfortheweb.org/process/) y [salvarla de la "distopía digital"](https://www.genbeta.com/actualidad/tim-berners-lee-creador-wold-wide-web-lanza-iniciativa-para-salvarla-distopia-digital).

Aunque para esto quizás también tendríamos que hablar primero de la necesidad de [una fuerte](https://www.0x65.dev/blog/2019-12-01/the-world-needs-cliqz-the-world-needs-more-search-engines.html) y [justa competencia en el mercado de los buscadores](https://www.xataka.com/servicios/google-nos-encierra-google-usuarios-no-hacen-clic-mitad-busquedas), para que así no esté tan normalizada la depedencia de empresas que viven de los datos. **¿Es Internet realmente un medio "justo" o finalmente han conseguido ponerle puertas al campo?**

[Arriba](#top)

### <a name="septima">Regalando cacharros espía por Navidad

La fiebre consumista en estas épocas del año puede llevarnos a tener la tentación de comprar el último cacharro tecnológico "smart", el problema es que tenemos que ser conscientes de que estamos metiendo un micrófono en nuestra casa que graba y almacena cada una de las conversaciones que podamos tener en la intimidad de nuestra propio hogar; aunque presuponiéramos que las empresas solo tengan intención de darle un uso justo a esas grabaciones (que ya es presuponer mucho), encontramos el problema de que estos dipositivos (al igual que la mayoría del IoT) [no son tan seguros como podríamos pensar](https://arstechnica.com/information-technology/2019/11/researchers-hack-siri-alexa-and-google-home-by-shining-lasers-at-them/).

Aunque no es solo problema de los "espías" que introducimos en nuestra casa, hay un auténtico problema con los "wearables", tengan función ["smart" más o menos compleja](https://www.reddit.com/r/privacy/comments/e2boh8/bose_headphones_are_basically_a_spyware_on_your/) o directamente tengan [todos los datos sobre nuestra estado de salud registrada día a día en todo momento](https://www.xataka.com/wearables/oficial-google-compra-fitbit-fabricante-wearables-2-100-millones-dolares). **¿Somos "tontos" por querer tener dispositivos "inteligentes"?***

[Arriba](#top)

### <a name="octava">El problema de que las personas "responsables" sean "irresponsables" en cuanto a seguridad informática

Desde el verano no paran de saltar noticias de [ayuntamientos](https://www.pandasecurity.com/spain/mediacenter/noticias/ransomware-ayuntamientos-espanoles/) y [más ayuntamientos que han sido infectados por ransomware](https://sietediasalhama.com/noticia/4234/sucesos/el-ayuntamiento-sufre-un-ciberataque-de-gravedad-extrema.html), pero también [emisoras de radio, grandes compañías de informática](https://www.xataka.com/seguridad/ciberataque-ransomware-deja-ko-sistemas-cadena-ser-everis) o [empresas de seguridad privada](https://www.elconfidencial.com/empresas/2019-11-27/prosegur-ciberataque-hacker-comunicado_2354800/).

Pero por otro lado, también vemos que los bancos cumplen poco más que los mínimos para aparentar seguridad en sus sistemas, y cuando se producen [filtraciones de datos dejan en peligro a las personas más vulnerables, los usuarios finales que confían en ellos](https://www.elconfidencial.com/tecnologia/2019-11-30/hackeo-ruralvia-clientes-datos-dni-clientes_2357727/). Esto, unido a que algunos [bancos obligan a sus clientes a descargarse su aplicación](https://www.ocu.org/dinero/cuenta-bancaria/noticias/ing-aplicacion-obligatoria), nos deja ante un panorama bastante desalentador.

**¿Son todas las compañías tan irresponsables en matería de seguridad informática como parece?¿Nadie les vas a exigir el mismo nivel de obligación que nos imponen en el cumplimiento?**

[Arriba](#top)

Hablaremos también de algunas iniciativas que queremos plantear para el futuro, y si nos sugerís alguna otra nos encantará planificarla o colaborar en su desarrollo y darle forma si es factible.

Además, si quieres también puedes hacer cualquier comentario sobre esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias) o en nuestro recién estrenado [foro](https://interferencias.tech/foro/).

**Toda persona que se quiera acercar y compartir sus ideas, hacer propuestas, discutir con el grupo o simplemente escuchar será bienvenida. Además, nos solemos ir de tapeo al terminar, por lo que siempre podemos seguir comentando cosas mientras tomamos algo; la gente nueva siempre es bien recibida. No mordemos 😅**

**Nos encontraremos el viernes 13 a las 19:00 en Biblioteca Social “Hermanos Quero” (Calle Acera del Triunfo, 27 ➡️ [https://www.openstreetmap.org/node/2055915287](https://www.openstreetmap.org/node/2055915287))**

### ¡Te esperamos!
