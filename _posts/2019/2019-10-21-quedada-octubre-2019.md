---
layout: post
title: Quedada Octubre 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: quedadas
---

De nuevo nos reunimos para hablar de diversos temas de actualidad en el mundo de los derechos digitales, la lucha por el software libre y nuestra lucha a favor de la privacidad y seguridad en el mundo tecnológico. Como siempre, las quedadas son abiertas a todo el mundo y cualquier persona puede proponer un tema sobre el que debatir, aunque de entrada sugerimos las siguientes:

- La sofisticación tecnológico que ha demostrado "Tsunami Democràtic" para coordinar las protestas sociales y su posterior intento de bloqueo:
    - [https://blog.elhacker.net/2019/10/todo-lo-que-quieres-saber-sobre-el-funcionamiento-tecnico-app-Tsunami-democractic-apk.html](https://blog.elhacker.net/2019/10/todo-lo-que-quieres-saber-sobre-el-funcionamiento-tecnico-app-Tsunami-democractic-apk.html)
    - [https://www.publico.es/politica/audiencia-nacional-investiga-tsunami-democratic.html](https://www.publico.es/politica/audiencia-nacional-investiga-tsunami-democratic.html)

- La detención de un "hacker" relacionado con la campaña #OpCatalonia de Anonymous en respuesta a las setencias del "procés":
    - [https://www.elconfidencial.com/tecnologia/2019-10-17/hacker-proces-sentencia-opcatalonia_2288040/](https://www.elconfidencial.com/tecnologia/2019-10-17/hacker-proces-sentencia-opcatalonia_2288040/)

- Google financió durante años a grupos negacionistas del cambio climático
    - [https://www.elconfidencial.com/tecnologia/2019-10-11/google-financia-negacionistas-cambio-climatico_2279039/](https://www.elconfidencial.com/tecnologia/2019-10-11/google-financia-negacionistas-cambio-climatico_2279039/)
    - [https://www.theguardian.com/environment/2019/oct/11/google-contributions-climate-change-deniers](https://www.theguardian.com/environment/2019/oct/11/google-contributions-climate-change-deniers)

- En Madrid han probado a pagar el billete de bus con la cara:
    - [https://elpais.com/ccaa/2019/10/03/madrid/1570115313_902888.html](https://elpais.com/ccaa/2019/10/03/madrid/1570115313_902888.html)

- China quiere imponer un test de reconocimiento facial para obtener un número de telefono:
    - [https://qz.com/1720832/china-introduces-facial-recognition-step-to-get-new-mobile-number/](https://qz.com/1720832/china-introduces-facial-recognition-step-to-get-new-mobile-number/)
    - [https://cincodias.elpais.com/cincodias/2019/10/04/lifestyle/1570183186_222841.html](https://cincodias.elpais.com/cincodias/2019/10/04/lifestyle/1570183186_222841.html)

- Administraciones públicas que paralizadas por ataques ransomware debido a la falta de concienciación sobre la importancia de la seguridad informática:
    - [https://www.xataka.com/seguridad/paralizan-ayuntamiento-jerez-encriptando-su-base-datos-virus-informatico-piden-rescate-para-liberarlo](https://cincodias.elpais.com/cincodias/2019/10/04/lifestyle/1570183186_222841.html)

- Edward Snowden explica cómo usa su smartphone:
    - [https://www.eleconomista.com.mx/tecnologia/Edward-Snowden-nos-dice-como-usa-su-smartphone-20190929-0002.html](https://www.eleconomista.com.mx/tecnologia/Edward-Snowden-nos-dice-como-usa-su-smartphone-20190929-0002.html)

- Multa de 300.000 euros a Vueling por no permitir rechazar sus cookies:
    - [https://www.publico.es/sociedad/proteccion-datos-multa-vueling-com-30000-euros-no-dar-posibilidad-oponerse-cookies.html](https://www.publico.es/sociedad/proteccion-datos-multa-vueling-com-30000-euros-no-dar-posibilidad-oponerse-cookies.html)

- El secuestro de mensajes de Signal, Telegram y WhatsApp:
    - [https://www.scmagazineuk.com/researchers-reveal-easily-signal-telegram-whatsapp-messages-hijacked/article/1521145](https://www.scmagazineuk.com/researchers-reveal-easily-signal-telegram-whatsapp-messages-hijacked/article/1521145)
    - [https://blog.segu-info.com.ar/2019/09/investigadores-revelan-que-se-pueden.html](https://blog.segu-info.com.ar/2019/09/investigadores-revelan-que-se-pueden.html)

- Acuerdo entre UK y USA para compartir los mensajes cifrados enviados por los usuarios de medios sociales por "motivos de seguridad":
    - [https://www.bloomberg.com/news/articles/2019-09-28/facebook-whatsapp-will-have-to-share-messages-with-u-k-police](https://www.bloomberg.com/news/articles/2019-09-28/facebook-whatsapp-will-have-to-share-messages-with-u-k-police)

- El buffer overflow que lleva haciendo vulnerables nuestros Linux por WiFi desde hace varios años:
    - [https://arstechnica.com/information-technology/2019/10/unpatched-linux-flaw-may-let-attackers-crash-or-compromise-nearby-devices/](https://arstechnica.com/information-technology/2019/10/unpatched-linux-flaw-may-let-attackers-crash-or-compromise-nearby-devices/)
    - [https://www.genbeta.com/actualidad/vulnerabilidad-linux-presente-2013-permite-atacar-dispositivos-via-wifi](https://www.genbeta.com/actualidad/vulnerabilidad-linux-presente-2013-permite-atacar-dispositivos-via-wifi)

- Alexa, el asistente virtual de Amazon, se sabía que era vulnerable...y lo sigue siendo:
    - [https://unaaldia.hispasec.com/2019/09/bypass-mediante-sql-injection-en-alexa.html](https://unaaldia.hispasec.com/2019/09/bypass-mediante-sql-injection-en-alexa.html)

Hablaremos también de algunas iniciativas que queremos plantear para el futuro, y si nos sugerís alguna otra nos encantará planificarla y darle forma si es factible.

Además, si quieres también puedes hacer cualquier comentario sobre esto en nuestro foro: [https://interferencias.tech/foro/t/quedada-interferenciaria-octubre/90](https://interferencias.tech/foro/t/quedada-interferenciaria-octubre/90)

Toda persona que se quiera acercar y compartir sus ideas, hacer propuestas, discutir con el grupo o simplemente escuchar será bienvenida. No mordemos (hay tapeo, así que nos entretenemos con eso), y la gente nueva siempre es bien recibida.

Nos encontraremos el sábado 26 a partir de las 19:00 en Cafetería Bar MundiSalado MundiDulce (Calle Gonzalo Gallas, 3 ➡️ [https://www.openstreetmap.org/node/6872873194](https://www.openstreetmap.org/node/6872873194))

¡Te esperamos!
