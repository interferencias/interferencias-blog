---
layout: post
title: Noticias semana 5 al 11 de agosto de 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

Estas son las noticias más relevantes que hemos encontrado en nuestras redes sociales esta semana:

- **China distributes spyware at its border and beyond**: [[https://www.wired.com/story/china-distributes-spyware-border](https://www.wired.com/story/china-distributes-spyware-border)
- **A Boeing code leak exposes security flaws deep in a 787's guts**: [https://www.wired.com/story/boeing-787-code-leak-security-flaws/](https://www.wired.com/story/boeing-787-code-leak-security-flaws/)
- **To Code or Not to Code: should lawyers learn to code?**: [https://lawtomated.com/to-code-or-not-to-code-should-lawyers-learn-to-code-3-2/](https://lawtomated.com/to-code-or-not-to-code-should-lawyers-learn-to-code-3-2/)
- **A new documentary and a local children’s book tackle Big Data’s dangers from different angles**: [https://www.dailymaverick.co.za/article/2019-07-30-a-new-documentary-and-a-local-childrens-book-tackle-big-datas-dangers-from-different-angles/](https://www.dailymaverick.co.za/article/2019-07-30-a-new-documentary-and-a-local-childrens-book-tackle-big-datas-dangers-from-different-angles/)
- **The Trump administration is using the full power of the U.S. surveillance state against whistleblowers**: [https://theintercept.com/2019/08/04/whistleblowers-surveillance-fbi-trump/](https://theintercept.com/2019/08/04/whistleblowers-surveillance-fbi-trump/)
- **How dissidents are using shortwave radio to broadcast news into China**: [https://www.defenseone.com/technology/2019/08/how-dissidents-are-using-shortwave-radio-broadcast-news-china/158950/](https://www.defenseone.com/technology/2019/08/how-dissidents-are-using-shortwave-radio-broadcast-news-china/158950/)
- **Africa: SIM card registration only increases monitoring and exclusion**: [https://www.privacyinternational.org/long-read/3109/africa-sim-card-registration-only-increases-monitoring-and-exclusion](https://www.privacyinternational.org/long-read/3109/africa-sim-card-registration-only-increases-monitoring-and-exclusion)
- **Better encrypted group chat**: [https://blog.trailofbits.com/2019/08/06/better-encrypted-group-chat/](https://blog.trailofbits.com/2019/08/06/better-encrypted-group-chat/)
- **Police arrest student leader over laser pointers**: [https://news.rthk.hk/rthk/en/component/k2/1473126-20190806.htm](https://news.rthk.hk/rthk/en/component/k2/1473126-20190806.htm)
- **2019 DuckDuckGo privacy donations: $600,000 for privacy advocacy**: [https://spreadprivacy.com/2019-duckduckgo-privacy-donations/](https://spreadprivacy.com/2019-duckduckgo-privacy-donations/)
- **Microsoft también escucha tus conversaciones de Skype y Cortana**: [https://hipertextual.com/2019/08/microsoft-tambien-escucha-tus-conversaciones-skype-cortana](https://hipertextual.com/2019/08/microsoft-tambien-escucha-tus-conversaciones-skype-cortana)
- **Who owns your wireless service? Crooks do**: [https://krebsonsecurity.com/2019/08/who-owns-your-wireless-service-crooks-do/](https://krebsonsecurity.com/2019/08/who-owns-your-wireless-service-crooks-do/)
- **Twitter ‘fesses up to more adtech leaks**: [https://techcrunch.com/2019/08/07/twitter-fesses-up-to-more-adtech-leaks/](https://techcrunch.com/2019/08/07/twitter-fesses-up-to-more-adtech-leaks/)
- **El último método para infiltrarse en una red informática no usa técnicas complicadas: paquetes físicos que llegan a casa**: [https://www.xataka.com/seguridad/ultimo-metodo-para-infiltrase-red-informatica-no-usa-tecnicas-complicadas-paquetes-fisicos-que-llegan-a-casa](https://www.xataka.com/seguridad/ultimo-metodo-para-infiltrase-red-informatica-no-usa-tecnicas-complicadas-paquetes-fisicos-que-llegan-a-casa)
- **The death of social reciprocity in the era of digital distraction**: [https://blogs.scientificamerican.com/observations/the-death-of-social-reciprocity-in-the-era-of-digital-distraction/](https://blogs.scientificamerican.com/observations/the-death-of-social-reciprocity-in-the-era-of-digital-distraction/)
- **Black Hat: GDPR privacy law exploited to reveal personal data**: [https://www.bbc.com/news/technology-49252501](https://www.bbc.com/news/technology-49252501)
- **I tried hiding from Silicon Valley in a pile of privacy gadgets**: [https://www.bloomberg.com/news/features/2019-08-08/i-tried-hiding-from-silicon-valley-in-a-pile-of-privacy-gadgets](https://www.bloomberg.com/news/features/2019-08-08/i-tried-hiding-from-silicon-valley-in-a-pile-of-privacy-gadgets)
- **Facebook loses facial recognition appeal, must face privacy class action**: [https://www.usnews.com/news/top-news/articles/2019-08-08/facebook-loses-facial-recognition-technology-appeal-must-face-class-action](https://www.usnews.com/news/top-news/articles/2019-08-08/facebook-loses-facial-recognition-technology-appeal-must-face-class-action)
- **El nuevo reto viral, “vecinos de número”, y los riesgos en privacidad**: [https://cybersecuritynews.es/el-nuevo-reto-viral-vecinos-de-numero-y-los-riesgos-en-privacidad/](https://cybersecuritynews.es/el-nuevo-reto-viral-vecinos-de-numero-y-los-riesgos-en-privacidad/)
- **¿Las protestas del futuro? Así 'hackean' en Hong Kong al 'Gran Hermano' chino**: [https://www.elconfidencial.com/tecnologia/2019-08-09/protestas-hong-kong-hackear-inteligencia-artificial_2169083/](https://www.elconfidencial.com/tecnologia/2019-08-09/protestas-hong-kong-hackear-inteligencia-artificial_2169083/)
- **Big Brother: Donald Trump prepararía orden ejecutiva para monitorear redes sociales**: [https://www.fayerwayer.com/2019/08/trump-orden-ejecutiva-redes-sociales/](https://www.fayerwayer.com/2019/08/trump-orden-ejecutiva-redes-sociales/)
- **Acusan a Microsoft de escuchar las llamadas de Skype y no especificarlo en sus condiciones**: [https://www.genbeta.com/actualidad/acusan-a-microsoft-escuchar-llamadas-skype-no-especificarlo-sus-condiciones](https://www.genbeta.com/actualidad/acusan-a-microsoft-escuchar-llamadas-skype-no-especificarlo-sus-condiciones)
- **Black Hat USA 2019: los hackers éticos deben proteger los derechos digitales**: [https://www.muyseguridad.net/2019/08/09/derechos-digitales-black-hat-2019/](https://www.muyseguridad.net/2019/08/09/derechos-digitales-black-hat-2019/)
- **Adolescentes de Instagram sacrifican su privacidad por obtener más datos**: [https://www.ticbeat.com/tecnologias/instagram-datos-privacidad/](https://www.ticbeat.com/tecnologias/instagram-datos-privacidad/)
- **We analysed the 527,350 Facebook Ads placed by the US presidential candidates. Here are the results.**: [https://medium.com/applied-data-science/56-070-165-facebook-ad-spend-of-us-presidential-candidates-broken-down-by-age-and-gender-2dcc32fe2c02](https://medium.com/applied-data-science/56-070-165-facebook-ad-spend-of-us-presidential-candidates-broken-down-by-age-and-gender-2dcc32fe2c02)
- **El Internet de las cosas nos rodea pero… ¿sabemos proteger nuestros datos?**: [https://www.ticbeat.com/tecnologias/el-internet-de-las-cosas-nos-rodea-pero-sabemos-proteger-nuestros-datos/](https://www.ticbeat.com/tecnologias/el-internet-de-las-cosas-nos-rodea-pero-sabemos-proteger-nuestros-datos/)
- **Black Hat talk about ‘Time AI’ causes uproar, is deleted by conference**: [https://www.vice.com/en_us/article/8xw9kp/black-hat-talk-about-time-ai-causes-uproar-is-deleted-by-conference](https://www.vice.com/en_us/article/8xw9kp/black-hat-talk-about-time-ai-causes-uproar-is-deleted-by-conference)
- **El dilema de cómo regular a los titanes digitales**: [https://elpais.com/economia/2019/08/10/actualidad/1565469638_196784.html](https://elpais.com/economia/2019/08/10/actualidad/1565469638_196784.html)

### Recomendado:
- **The Hacker's Hardware Toolkit** por [Yago Hansen](https://twitter.com/yadox/status/1158435412257513472), una colección de dispositivos de hardware para Red Team hackers, pentesters e investigadores de seguridad: [https://github.com/yadox666/The-Hackers-Hardware-Toolkit/](https://github.com/yadox666/The-Hackers-Hardware-Toolkit/)

### Redes sociales:
- **Hilo de [David Marugán](https://twitter.com/RadioHacking/) con consejos sobre seguridad operacional en viajes**: [https://twitter.com/RadioHacking/status/1101946466598309888](https://twitter.com/RadioHacking/status/1101946466598309888)

Recuerda que además de poder seguirnos en [Twitter](https://twitter.com/Inter_ferencias) y [Mastodon](https://mastodon.technology/@interferencias), te invitamos a participar y comentar sobre todo esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias).
