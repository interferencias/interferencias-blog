---
layout: post
title: Why anti-encryption laws are crazy
author: terceranexus6
image:
  feature: banners/header.jpg
tags: encriptación cifrado seguridad
---

<img src="{{ site.url }}/assets/images/dev.to/200625/01.webp" style="display: block; margin: 0 auto;">

## Introduction

Since tech became open and community guided, the fear of developing tools for the users and not for the companies or the government has grown bigger. In recent [news](https://www.eff.org/deeplinks/2020/06/senates-new-anti-encryption-bill-even-worse-earn-it-and-thats-saying-something) a new bill is presented in which Justice Department will be able to require manufacturers of OS and devices as well as communication providers the ability to decrypt data upon request.

The whole point of encryption is to **ensure the security of your information**. Requiring your information to be safe is not just a matter of “hiding” stuff. Several cyberattacks push Blue Teams and companies to keep their security in its peak, backdoors are a risk to everyone’s information. An encryption with a backdoor is no longer a trustworthy encryption, it might be a hard-to-open door, that’s all. Let’s explore a bit about encryption to make sense of this:

## From Diffie and Hellman to RSA

If you guys ever had a calculus or algorithms subject you might have heard these names already. Whitfield Diffie and Martin Hellman are the cryptologist that name the method for securely exchanging cryptographic keys. The whole, basic idea of this method is to create a shared key without the intervention of third parties for obvious reasons, but it had it flaws as it’s based in **symmetric keys**. It’s still useful for many things, but RSA was based in the same idea using asymmetric keys. I had to calculate **RSA** with pen and paper at university and it’s a pain in the ass, so I will shortcut and give the general recipe:

* Using a couple of large **prime numbers** and keep them secret, using randomness, which by the way isn’t that easy, I will explain later.
* The modulus for public and private keys in n is defined as part of the public key.
* We involve some magic called **Carmichael’s totient function** and **Euclidean algorithm**. Using a fast duckduckgo search we can create an idea of both, but the whole point is to have a solid computable method for defining the private key so it’s a one-way calculus and there’s no way of getting it back from the information provided, in case a naughty MiTM is happening (which was a real concern in Diffie-Hellman).

Actually the main idea is to craft a **modular multiplicative inverse**, the main part of the private key, that can be used to resolve all, but this process is not compromised, and actually all the information used to calculate this value is disposable.

<img src="{{ site.url }}/assets/images/dev.to/200625/02.gif" style="display: block; margin: 0 auto;">

Anyway after all this gibberish, **the important fact here is that we craft a double-key that can’t be decrypted using the information of the public part** (the shared one) as that part is a one way-ticket.

## Randomness

We previously said that creating true randomness is not that easy, right? Well randomness is an important part of the process, so let me explain. If we think of a limit from the bottom (0 for example) to the top (let’s stay classy and say 10) and you are asked to randomly choose a number you could say anything (did you think of 7? Hah I knew it) and that might be considered random but let’s remember computers are very stupid creatures that depends on clear commands to work. So “programming” randomness depend on choosing random facts so the computer can craft some randomness.

<img src="{{ site.url }}/assets/images/dev.to/200625/03.gif" style="display: block; margin: 0 auto;">

This is why when we create a public-private key using GPG we might be asked to move the mouse all around, open and close stuff and similar, **the computer is trusting on your human randomness**. This is safe enough for that case, but let’s think of a situation in which we need randomness and human interaction is not an option. In my case I’m thinking of an Arduino IoT project or similar. A formal, new generation computer would be able to do the work just fine, but for example in IoT we will need specific chips for encryption, mine is **ATECC608A**, that includes (*tadah tadah*) Elliptic Curve Diffie-Hellman. The idea is the same, is to perform one-way-ticket public and super safe private encryption embedded in a project. **If encryption was supposed to be decrypted for anyone who commands it, do you think all this effort would be necessary?**

At this point you might already think what I was trying to explain at first: **encryption is designed to be unbreakable on command**, it’s the whole point. Not only due to the trust in this third person, but due to the method itself. Do you see any step in the process involving “*oh and create a copy key to this very trustworthy third party!*” **no**, because it makes literally no sense in theory. It doesn’t matter how much you trust this third party, accepting these terms involves:

* Compromising your security even more, as your safety not only depends on being attacked, but it also depends on you trustworthy third party being attacked.
* Changing almost 25 years of hard work made by mathematicians in order to keep you safe to literally make the internet more dangerous.

So, think about this when trying to build an opinion on how important encryption is. **Anything that tries to break our safety for whichever reasons have to be at least, subject of suspicion**.

*Also written in: [https://dev.to/terceranexus6/why-anti-encryption-laws-are-crazy-188k](https://dev.to/terceranexus6/why-anti-encryption-laws-are-crazy-188k)*
