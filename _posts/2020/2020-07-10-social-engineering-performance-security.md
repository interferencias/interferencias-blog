---
layout: post
title: Social Engineering&#58 The performance security
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad
---

<img src="{{ site.url }}/assets/images/dev.to/200710/01.webp" style="display: block; margin: 0 auto;">

Disclaimer: All of the following information takes for granted you've been hired for it.

One of the best parts of offensive security is the wide range of attacks than can be performed. As the idea is to simulate a real cyberattack, there are several kind of things to do to approach the "victim". Even most of it involves using various tools in the computer, it also implies physical access, phishing, and getting information though the users of the system. This parts involves a bit of a performance, to learn about what kind of people work there, what kind of clients do they have, etc. It's helpful for either simulate being one of those or craft personalized attacks. For example, if you know that most of the staff use a specific tech brand, you can create a malicious gift (a pendrive, link, program, etc) which also requires some serious crafting, how good are you with stickers and presentation? ;)

Anyway when it comes to emulate other kind of person, there are many useful things you can use. When speaking through the phone, or in person, you need to have clear your basic information (name, age, personal details, contextual...) so first of all is building a character. The character could change depending on the situation, there are some random character generators out there, but you might want to create a more specific character.

I remember I had this job interview time ago in which saying I used to roleplay fiction with my friends was a nice flag. Since I was a kid I had to study contextual details and facial expressions for personal reasons, and apparently it came in handy for my professional development. Understanding how the people in a company works, communicate and perform is a huge tip into understanding what kind of flaws they might have. Also you have to know your limitations, as much as I would like to perform some roles, I have to contextualize my complexity, my voice and my general looks. I once asked a junior to act as my boss as he had this very strong voice, and I made myself a role as a junior (I was senior of that project) due to my soft voice, and it worked.

Investigating the people in a company includes:

* **Learning about their structure** (do they have separates offices or do they work all together?, do they have to dress formal? do they have managers, bosses, juniors, seniors or other kind of organization?) which sometimes is just a matter of browsing Linkedin, official website and social media.
* **Learning about their routines** (do they have breakfast at the same place everyday? do they go outside the building for that? do they know each other or rotate so they have different workmates from time to time?) This information could lead into attacks as the one Naomi Wu did in China, in which she met a guy from a company who used to have coffee in front of the company building, or use sniffing devices.
* **Learning about their tech** (do they use windows? apple? do they bring laptops outside or the equipment is inside the building all the time?)
* **Learning about the clients** (are they used to attend callings from clients? do they have review pages that could give us context on certain issues that happened before?) This could give you context to work with, referring to that issue plus some public information (NIF for example) could make people think you are this client for real.

<img src="{{ site.url }}/assets/images/dev.to/200710/02.gif" style="display: block; margin: 0 auto;">

Of course believing your own story is important, so if you are using a phone is wise to have a *cheatsheet* in front of you with your fake information, and cautiously save it for afterwards in case you are successful and you need to use that character again. For example: name, ID, age, birth place, favourite color, pet, anything that you think is needed. Even using fake sounds such as a crying baby (a very useful resource to create a hurry-up environment), a barking dog or anything like that. Creating those kind of environments could be useful for gathering information that shouldn't be released: for example in the case I mentioned before I was a "fucked up junior" in a Friday evening (when everyone wants to finish already). Every detail is important.

Using different phone numbers also allows you to dissociate your character from your professional number. If you are perfectionist enough you can create some fake profiles beforehand in social media in order to refer to them if the chance comes. But the whole thing is more about coming up with things naturally even in the most weird situations. I read about a useful schematic into character creation for Social engineering when I was studying for readteam some years ago and the woman authoring even had rings, accessories and outfit for specific situations. Actually it's all about how far are you willing to go. I have some friends who had to craft a pen-drive present and we laughed about being crafty enough for making it cute and passable. There's this guy (not friend of mine, but he gave a speech about it) who even asked his mom to collaborate for an attack into a prison. Not gonna lie, the hell of a fun job.

<img src="{{ site.url }}/assets/images/dev.to/200710/03.gif" style="display: block; margin: 0 auto;">

Obviously we are not willing to cause trouble to anyone, so when we write about it afterwards we try to generally speak about the attack and don't point at anyone. It's important to focus on improving the security measures instead on the specific people. The idea is to afterward suggest how to improve the security methodology in the customer assistance and daily performance so everyone is at least informed into how to react against different situations. So... here you go some tips for avoid falling into this:

<img src="{{ site.url }}/assets/images/dev.to/200710/04.gif" style="display: block; margin: 0 auto;">

* If a hurry up email, calling or customer appears on anyday, but even more on Friday afternoon, don't panic, slow down and calmly try to resolve the problem.
* Never ever ever discuss personal details or relevant information without letting your responsible know and NEVER through a non-safe channel. Never never change password through phone calls or anything. Even if it sounds like a really hurry-up situations. actually *specially* if so.
* If someone you don't know acts like is working there, be cautious with the information disclosure and don't let this person know important details until you are referenced by other workmates. Try to train yourself into what kind of information you can and cannot disclosure. My family sometimes ask me about my job and I clearly know what I can and cannot say, not because of them, but because of who else is listening.
* Careful with very cool presents at work. EXTREMELY careful with pendrives. set it ON FIRE
* If you see someone putting a RPI in your Network is probably not the cleaner. Call security. (There are some workmates that had to deal with detention more than once) this person might be a friendly redteam fella, or a real attacker, in both cases you need to tell security. We will deal.

*Also written in: [https://dev.to/terceranexus6/social-engineering-the-performance-security-5g8l](https://dev.to/terceranexus6/social-engineering-the-performance-security-5g8l)*
