---
layout: post
title: Aplazamiento indefinido JASYP '20

author: germaaan
image:
  feature: banners/header.jpg
tags: eventos
---

Lamentamos tener que comunicar que hemos decidido aplazar de forma indefinida las JASYP ‘20 que estaban planificadas para los días 17 y 18 de abril, dado que por la situación actual, lo más correcto es no continuar con la organización de las mismas mientras que no se puede asegurar que no existe ningún riesgo para la salud pública por el COVID-19.

La intención es que las Jornadas se realicen en algún momento de los próximos meses, pero es algo que todavía no podemos concretar. Os informaremos cuando tengamos más noticias.

Sentimos mucho los inconvenientes generados.
