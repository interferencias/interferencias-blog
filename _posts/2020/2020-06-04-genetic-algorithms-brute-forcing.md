---
layout: post
title: Genetic algorithms for brute forcing
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad cpp bash
---

<img src="{{ site.url }}/assets/images/dev.to/200604/01.webp" style="display: block; margin: 0 auto;">

Due to my work I tend to use brute force lists a lot, mostly to test incorrect login management. Sometimes I feel I get really close to correct passwords either guessing or using profiling tools. I don't know if some of you, when it comes to change the default or old passwords you just take your old one and change it a bit. That's mostly wrong btw.

In any case recently I felt like hand-writing customized lists out of guessing on a text file was a poorly decision, so I decided to create a command that creates fitted guessing options from a fitting range using a genetic algorithm. I created a [GitLab repo](https://gitlab.com/terceranexus6/jock_pass/-/tree/master) in order to share it and I'd like to improve it, mostly because I know C++ might not be the best option and also because I'm open to suggestions, in general.

If you are curious please check and merge request your ideas!

Example of use with a sample guessed password and the desired fitness 2:

```
$ ./jockpass myPasswordGu3ss 2
```

<img src="{{ site.url }}/assets/images/dev.to/200604/02.gif" style="display: block; margin: 0 auto;">

*Also written in: [https://dev.to/terceranexus6/genetic-algorithms-for-brute-forcing-4e6j](https://dev.to/terceranexus6/genetic-algorithms-for-brute-forcing-4e6j)*
