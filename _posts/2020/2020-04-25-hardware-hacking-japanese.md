---
layout: post
title: ハードウェアのハッキング
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad hardware
---

私の専門 はハードウェアのハッキング！ 電子機器を設計を楽しんでいます。基本的な物理学を復習しましょう、お先に 。

回路は電気の元(Vcc)とGND(0V)と電子部品をしています。抵抗器はとても大事、回路を壊さないでために抵抗器がつかうなければならない。

<img src="{{ site.url }}/assets/images/dev.to/200425/01.jpg" style="display: block; margin: 0 auto;">

ハッキング ために電子機器を設計は楽しいですしかしむずかしいですよ。はじめに、サイバー攻撃を考えてなければなrない。Bluetoothプロファイルの一覧はありますか?、ＷｉＦｉ 【ワイファイ】はありますか?

* 複雑の プロセシング を使て(ために)、RASPBERRY PIはとてもいいです。ネットワークの捕獲の仕掛けために私はRASPERRY PI ４ もとZEROを使て。
* ARDUINOはRASPBERRYより簡単です。でも、 PENDRIVEのバックドアのサイバー攻撃 ためにいいです。(ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ [ コンピュータを注視 ほうがいいですよ ]*:･ﾟ✧

便利電子部品:

* [ESP8266 Bluetooth](https://www.adafruit.com/product/2471)とWIFIあります
* [RPI Zero W WIFI](https://www.adafruit.com/product/3400)あります
* [BLUE Fruit Sniffer Bluetooth](https://www.adafruit.com/product/2269)の スニファ
* [Digispark Attiny85 PENDRIVE](https://www.gearbest.com/other-accessories/pp_227676.html?lkid=15666791)のバックドア

<img src="{{ site.url }}/assets/images/dev.to/200425/02.jpg" style="display: block; margin: 0 auto;">

*Also written in: [https://dev.to/terceranexus6/-2gcp](https://dev.to/terceranexus6/-2gcp)*
