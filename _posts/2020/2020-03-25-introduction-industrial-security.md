---
layout: post
title: Introduction to industrial security
author: terceranexus6
image:
  feature: banners/header.jpg
tags: ot seguridad
---

<img src="{{ site.url }}/assets/images/dev.to/200325/87f44f80ab61.webp" style="display: block; margin: 0 auto;">

## A tale of spies and (cyber)war

It was the start of 2010, Iran. The Atomic Energy Agency noticed something was wrong in the Natanz uranium enrichment plant. It seemed weird, as centrifuges that were used to enrich uranium gas were starting to fail. They decided to replace them. On summer of that year, computers in the Iranian plant started to fail and reboot on their own and no one knew what was going on. Imagine being the system administrator and feeling all that chaos and not knowing what's going on. The nightmare become real when they discovered the malware: the were facing **Stuxnet**.

<img src="{{ site.url }}/assets/images/dev.to/200325/stuxnet.png.jpg" style="display: block; margin: 0 auto;">

*In the picture above, Iranian President Mahmoud Ahmadinejad watching the bug in the screens of the plant.*

Stuxnet was meant to make centrifuges fail slowly until crashing, the attacker (it was completely targeted, not a random attack) wanted it to fail and close the plant, not to create a big mess and kill a lot of people. Political will was obvious in this case.

Even though Stuxnet is the most famous example, attacks against Operational Technology is very common, in fact the majority of the targeted attacks are pointed to Critical Infrastructures. Either for political or economical reasons, this is a major threat.

## Pentesting OT

For this reason, as the cyber professionals came into the scene, the tools aimed for **pentesting** OT has increased. I've had the opportunity to investigate about this topic and I want to share some information about it.

### Enumerate OT devices

Enumeration is a very important part in pentesting, it help us focus our attacks and create targets. When speaking about OT, there are some devices that usually comes up. Firstly we need to know about ICS (industrial control systems) such as SCADA (Supervisory control and data acquisition) or PLC (programmable logic controller). But not just that, we also focus on robots, Operating systems and more. But there's a limited number of vendors of this kind of devices, so one thing we can do is to check for some of those vendors, such as Siemens, Modicon, Nigara or Phoenix, among others. We don't need to start with fancy stuff, **nmap** is useful for this matter, we only need to add some of the modules community designed for this purpose. For the ones who don't know nmap, is a very useful tool for scanning networks and look for open ports and more. For this purpose **Shodan** is also very useful, but using Shodan just like that might be tedious, si there are a couple of frameworks that use a Shodan API for OT pentesting, I'm going to point to **Kamerka**. Kamerka uses Python, Redis and Celery to create an user-friendly framework for OT devices scanning and graphical output.

### Attacking

There are several ways to attack a system, and approaching to the right way involves doing a good job in the previous part. Even though there are some modules in already well known tools such as **Metasploit**, a good attack involves creating scripts and getting your hands dirty, sometimes even physically going to the building and testing several things. In any case, enumeration is crucial.

### To sum up

Protecting industrial structures means protecting resources and persons. It's a different approach than directly protecting users in servers and computers but it's tightly related, as well as crucial due to the high rate of targeted attacks.

*Also written in: [https://dev.to/terceranexus6/introduction-to-industrial-security-119a](https://dev.to/terceranexus6/introduction-to-industrial-security-119a)*
