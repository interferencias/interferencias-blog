---
layout: post
title: Safety on the streets with Python
author: terceranexus6
image:
  feature: banners/header.jpg
tags: python seguridad
---

<img src="{{ site.url }}/assets/images/dev.to/200723/01.jpg" style="display: block; margin: 0 auto;">

For a few years I've been thinking in ways tech could help neighbourhoods into becoming more safe taking advantage of the people collaboration. It first came to me after realizing a neighbour a loved in my home town had some communication problems with the essential services. The whole idea of mine is logistically difficult to perform as so I ended up just theorising for a long time without actually writing a single line of code for that idea. Recently, after a discussion in [Mastodon](https://joinmastodon.org), I realized **there are some tiny goals to make public spaces safer** and that we could start from there.

<img src="{{ site.url }}/assets/images/dev.to/200723/02.gif" style="display: block; margin: 0 auto;">

I've been curious about **Open Street Map** for a while, so I decided I could work in my spare time between projects in a simple python code that created a OSM with marks. The whole idea was to create a launching portal (static, at first) with a map-launching button, and the map contains marks with dangers around the neighbourhood. The warnings could be from to broke asphalt to unsafe LBGTIQ spaces. Or! in a more positive sight, safe spaces! (**safe LBGTIQ** shops and meeting points, **feminist friendly** spaces, etc) as well as identifying trigger spaces such as betting places/alcohol stores. It kind of depends on the needs of each neighbourhood. It only needs a JSON (there's an example already) where you have to set location and the advise.

<img src="{{ site.url }}/assets/images/dev.to/200723/03.gif" style="display: block; margin: 0 auto;">

The idea is for the people to be able to upload information through **email or hashtags**, some people already suggested to add a form and some of them are planning on merge request that option. I'm so happy about how many people from my city wanted to help as soon as I launched the repo and I decided to share it here in case you guys want to use it in your neighbourhoods as well, or in any case modify the [repo](https://gitlab.com/terceranexus6/barriocheck).

<img src="{{ site.url }}/assets/images/dev.to/200723/04.gif" style="display: block; margin: 0 auto;">

Have fun and stay safe! I believe in the collaboration of the people for creating safer cities.

Right now [it's in Spanish](https://gitlab.com/terceranexus6/barriocheck), as I thought it would help local groups to better understand it, but I hope to soon add the English translation.

*Also written in: [https://dev.to/terceranexus6/social-engineering-the-performance-security-5g8l](https://dev.to/terceranexus6/social-engineering-the-performance-security-5g8l)*
