---
layout: post
title: The point of freedom
author: terceranexus6
image:
  feature: banners/header.jpg
tags: abierto libre software debate
---

Hello guys, how are you doing? I've had in my mind a topic for a while now and I'd love to share and hear from you about it. It's not quite technical but it's important in any techie job, I think.

Not long ago, [DEV went open source](https://dev.to/ben/devto-is-now-open-source-5n1) and it was pretty exciting for the ones who were following the development of the site. Some of us are very used to creating free and open content and happily joins Linux discussions, events and such. But, what's the point on this, why are we in this train? I'd like to start explaining the difference between free and open before going further, if I may.

Essentially, the freedom of the software is defined by four rules stated by Richard Stallman in the 80's. As explained in the [GNU manifesto](https://www.gnu.org/philosophy/free-sw.html), the four rules are:

<img src="{{ site.url }}/assets/images/dev.to/heckert_gnu.transp.small.png" style="display: block; margin: 0 auto;">

- The freedom to run the program as you wish, for any purpose (freedom 0).
- The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
- The freedom to redistribute copies so you can help others (freedom 2).
- The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

And yes, it starts at *zero*, because we all know how arrays work ;) And it sounds great! It talks about improving, sharing, learning...

<img src="{{ site.url }}/assets/images/dev.to/opensource.jpeg" style="display: block; margin: 0 auto;">

**Open source** is a term, as described in the [official source](https://opensource.org/osd) that hands the methodology for code distribution following the legal requirements in order to make it accessible to the community. Both have enriched the software development, it's said tho that freedom of the software takes a deeper meaning, more like a *lifestyle* or a *way of thinking*.

Now, I'd love to introduce some doubts, thoughts and question that have been in my mind for a while.

As I see it, we live in a very quick, frenetic routine that makes us enroll in a fast-thinking dynamic. Your opinions has to be sum up in a fast-readable tweet (best with a picture!) a fast-readable article, an elevator pitch... which is kind of a normal output out of a massive information society, but it has a dangerous trap; Fast culture shorten our deliberations, too. Why am I pointing at this now? Well, sometimes, in certain situations I've seen Freedom of software being used as a tag in order to make something fancier, or make it look like *coder friendly*, community friendly, without actually applying the basic spirit behind the concept. I want to make it clear, I'm not stating freedom of software is some kind of boys scouts badge or something like that, but **do we really mean it when we work on it?**

The entire movement of **freedom of the software** bases its points in the community awareness, the evolution of code as something not individual. But also something that should not be owned. The distribution of contributions aims for the code and the development instead for the personal scope. Making every single commit worth a review create greater, wider projects. This requires a pulse, a live community that not only create, but improve and critic all the time. **Critics are needed** in order to not fall in the conformity of the absolute rightness, improvement is impossible when there's no criticism. **What happens when freedom of the software is used as a label of rightness, a mere adjective, doesn't it lose its entity?**

This same concept broke the virtual wall, and arrived in the hardware community. Freedom of the hardware, leaded by those so called Makers, made possible amazing things, projects and ideas which are exciting and allow almost anyone to take part in electronics, something not as easy years ago. Actually, I've seen something in the makers communities that I haven't found that easily in software communities: greater multidisciplinary members. **Has this happened to any of you?** Many artist involved in tech are enrolled in maker community for example, also educators, even psychologists.

I firmly believe in tech as something any kind of professional should be able to take part in, actually why not? **We all already took our role as users, and took part in the digital world, why stay away from its development? Do you think any kind of professional is able to enrich the developers community?**

Hope to hear from you, guys!

*Also written in: [https://dev.to/terceranexus6/the-point-of-freedom-1i68](https://dev.to/terceranexus6/the-point-of-freedom-1i68)*
